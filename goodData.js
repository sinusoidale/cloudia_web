
//Prend une liste de donnée avec date et enlève les données abérante
//Un tableau avec les bonnes données est envoyer
//Algorithem  : http://fr.m.wikihow.com/calculer-des-données-aberrantes

//Format var badData = {"date":"21","date":"4534"}
//badData --> 

//Array for data
//var data = {50: "0",22: "1", 44: "2", 10:"3", 12:"4"};
var data = [50, 7, 7, 10, 8,7, 7, 10, 8,7, 7, 10, 8,7, 7,7, 7,7, 7,7, 7,7, 7,7, 7,7, 7,7, 7, 10, 8,7, 7, 10, 8,7, 7, 10, 8,7, 7, 10, 8,7, 7, 10, 8,7, 7, 10, 8];
function goodData(badData)
{

	//Mise en ordre de la plage de donnes
	var ordre = mergeSort(data);
	console.log(ordre);

	//Calculez la médiane de l’échantillon
	var position = parseInt(ordre.length/2);
	var mediane = (ordre[position] + ordre[position+1])/2;

	//Calculez le quartile inférieur
	var inférieur = parseInt(position/2);
	var quartileInférieur = (ordre[inférieur] + ordre[inférieur+1])/2;

	//Calculez le quartile supérieur
	var supérieur = parseInt(position * 1.5);
	var quartileSupérieur = (ordre[supérieur] + ordre[supérieur+1])/2;

	//étendue interquartile
	var interquartile = (quartileSupérieur - quartileInférieur);

	//limites extérieur
	var équart = interquartile * 5;
	var limitesSupérieur = quartileSupérieur + équart;
	var limitesInferieur = quartileInférieur - équart;

	console.log(limitesSupérieur);
	console.log(limitesInferieur);

	var goodData  = [];

	for(var i = 0; i < data.length; i++)
	{
		if(data[i] > limitesSupérieur || data[i] < limitesInferieur)
		{
			console.log("Donnée abérante : " + data[i]);
		}else
		{
			goodData.push(data[i]);
		}
	}

	//Retourn les bonne données
	return goodData;
}


//SOURCE -- > http://www.stoimen.com/blog/2010/07/02/friday-algorithms-javascript-merge-sort/
//Fonction qui met en ordre les données
//La fonction est O(n log n)
//On ne peut pas simplement utiliser la fonction sort en javascript puisque chaque navigateur utilise 
//différent algorithme donc le processigne devien beaucoup plus long.
function mergeSort(arr)
{
	if (arr.length < 2)
       	return arr;
 
    var middle = parseInt(arr.length / 2);
    var left   = arr.slice(0, middle);
    var right  = arr.slice(middle, arr.length);
 
    return merge(mergeSort(left), mergeSort(right));	
}

//Fusion le côté gauche et le côté droit
function merge(left, right)
{
    var result = [];
 
    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
 
    while (left.length)
        result.push(left.shift());
 
    while (right.length)
        result.push(right.shift());
 
    return result;
}
