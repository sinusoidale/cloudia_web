var mysql = require('mysql');

module.exports = mysql.createConnection({
  host     : 'localhost',							//Ne pas oublier de changer le host a localhost :)
  user     : 'root',
  password : '',
  database : 'cloudia'
});