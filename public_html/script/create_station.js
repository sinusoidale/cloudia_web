/****************************************
Fichier : create_station.js
Auteur : Christopher Brown
Fonctionnalité : Scripts pour que l'utilisateur créer une nouvelle station.
Date : 2017-04-30

Vérification :
Date	Nom		Approuvé
=========================================================
Historique de modifications :
Date	Nom		Description
=========================================================
****************************************/

//Envoit en forme de select tous les pays dans la base de données
$.getJSON( '/API/getStationCountry', function( data ) {
		if(data["state"] === "Success")
		{	
			
			var array = data["country"];
			var array2 = data["id_country"];

			var selectList = document.createElement("select");
			selectList.id = "country_select";
			selectList.className = "form-control";

			for (var i = 0; i < array.length; i++) {
			var option = document.createElement("option");
				option.value = array2[i];
				option.text = array[i];
				selectList.appendChild(option);
			}
			
			var myDiv = document.getElementById("country");
			myDiv.appendChild(selectList);

			selectList.addEventListener("change", function() {changeCity(selectList.value)}, false); 
			
			
		} else {
			//Request fail
			console.log("Failed to get countrys");
		}
			
});

//Envoit en forme de select les villes selon le pays sélectionné
function changeCity(selected) {
	$.getJSON( '/API/getStationCity?id='+selected, function( data ) {
			if(data["state"] === "Success")
			{	
		
				var array = data["city"];
				var array2 = data["id_city"];
				
				var city = document.getElementById("city");
				city.options.length=0;
				
				for (var i = 0; i < array.length; i++) {
					var option = new Option(array[i], array2[i]);
					city.options.add(option);
				}
				
			} else {
				//Request fail
				console.log("Failed to get citys");
			}	
	});
}

//Recherche les types de station pour mettre à jour le select station type
$.getJSON( '/API/getStationType', function( data ) {
		if(data["state"] === "Success")
		{	
			
			var array = data["stationType"];
			var array2 = data["id_station"];

			var station_type = document.getElementById("station_type");
	
			for (var i = 0; i < array.length; i++) {
				var option = new Option(array[i], array2[i]);
				station_type.options.add(option);
			}
			
		} else {
			//Request fail
			console.log("Failed to get countrys");
		}
			
});

//Recherche les types de station pour mettre à jour le select station type
$.getJSON( '/API/getStationGroup', function( data ) {
		if(data["state"] === "Success")
		{	
			
			var array = data["stationGroup"];
			var array2 = data["id_station"];

			var station_group = document.getElementById("station_group");
	
			for (var i = 0; i < array.length; i++) {
				var option = new Option(array[i], array2[i]);
				station_group.options.add(option);
			}
			
		} else {
			//Request fail
			console.log("Failed to get countrys");
		}
			
});