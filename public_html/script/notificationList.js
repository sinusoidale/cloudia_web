
// Fichier: notification.js
// Auteur: Guillaume Bergs
// Fonctionnalit�: W041 Syst�me d�alerte 
// Date: 2017-05-26
// V�rification: Date               Nom       
// =========================================================
// Historique de modifications:
// Date               Nom                   Description
// =========================================================
// 
//
//var connection = require('../../API/getInfo.js');
var nbAlertes = 0;
$.getJSON( '/API/getAlerts?id=11&unread=1', function( data ) {

	//Api request is a success or not
	if(data["state"] === "Success")
	{
		//Request Succes
		nbAlertes = data["result"].length;		var a;		var li;		var div;		var img;				var notificationBar; //li		var notificationBarContent; //ul		var checkedStatus; //li		var dataBar; //ul		var hour; //li		var sensorName; //li		var stationName; //li		var value; //li		var unit; //li		var reason; //li						var source = document.getElementById('notificationSpace');
		for(i = 1; i <= nbAlertes; i++){						//create the divider			div = document.createElement('li');			div.classList.add('divider');						//create the line for the alert			notificationBar = document.createElement('li');						//create the container for the data of the alert			notificationBarContent = document.createElement('ul');						//create the container for the information about the alert			dataBar = document.createElement('ul');						//create each element			//add the data: fetch it with the API call			//append it to the data bar							//Seen status					img = document.createElement('img');					if(data["result"][i].active == 1)						img.src = "../assets/pictograms/blue_bell.png";					else						img.src = "../assets/pictograms/white_bell.png";					img.style["max-width"] = "20px"					img.style["height"] = "auto"					dataBar.appendChild(img);											//country				sensorName = document.createElement('li');				sensorName.appendChild(document.createTextNode(data["result"][i].country_name));				dataBar.appendChild(sensorName);								//city				sensorName = document.createElement('li');				sensorName.appendChild(document.createTextNode(data["result"][i].city_name));				dataBar.appendChild(sensorName);								//station name				sensorName = document.createElement('li');				sensorName.appendChild(document.createTextNode(data["result"][i].station_name));				dataBar.appendChild(sensorName);							//hour				hour = document.createElement('li');				hour.appendChild(document.createTextNode(data["result"][i].date_time.replace(/([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2}).*/,"$1 $2")));				dataBar.appendChild(hour);								//name of sensor				sensorName = document.createElement('li');				sensorName.appendChild(document.createTextNode(data["result"][i].sensor_name));				dataBar.appendChild(sensorName);				//value				value = document.createElement('li');				value.appendChild(document.createTextNode(data["result"][i].measure_value));				dataBar.appendChild(value);				//unit				unit  = document.createElement('li');				unit.appendChild(document.createTextNode(data["result"][i].measure_name));				dataBar.appendChild(unit);				//reason				//reason = document.createElement('li');				//reason.appendChild(document.createTextNode(data["result"][i].nom_comparateur + " " + data["result"][i].seuil));				//dataBar.appendChild(reason);								//Append the read button				if(data["result"][i].active == 1){					//create the image					img = document.createElement('img');					img.src = "../assets/pictograms/read.png";					img.style["max-width"] = "20px"					img.style["height"] = "auto"										//create the associated link					a = document.createElement('a');					a.href = "#";					a.id = data["result"][i].id_alerte;					a.onclick = function() { 						var alertId = $(this).attr('id');						$.getJSON('/API/readAlert?id='+alertId, function( data ){;});						};										//append all					a.appendChild(img);					dataBar.appendChild(a);				}				//Append the delete button					//create the image					img = document.createElement('img');					img.src = "../assets/pictograms/delete.png";					img.style["max-width"] = "20px"					img.style["height"] = "auto"											//create the associated link					a = document.createElement('a');					a.href = "#";					a.id = data["result"][i].id_alerte;					a.onclick = function() { 						var alertId = $(this).attr('id');						$.getJSON('/API/deleteAlert?id='+alertId, function( data ){;});						this.parent.parent.remove(this.parent);						};										//append all					a.appendChild(img);					dataBar.appendChild(a);			//Append data bar to the notification bar content			notificationBarContent.appendChild(dataBar);															//Append the notificationBarContent to the notificationBar			notificationBar.appendChild(notificationBarContent);						//Append the notification to the dropdown menu			source.parentNode.insertBefore(notificationBar,source);						//append the divider			source.parentNode.insertBefore(div, source);
		}		
	}
	else
	{
		//Request fail
		console.log("Fail to load");
	}	
});
