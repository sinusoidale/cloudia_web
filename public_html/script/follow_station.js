/****************************************
Fichier : follow_station.js
Auteur : Christopher Brown
Fonctionnalité : Scripts pour que l'utilisateur choisisse une nouvelle station à suivre.
Date : 2017-04-19

Vérification :
Date	Nom		Approuvé
=========================================================
Historique de modifications :
Date	Nom		Description
2017-04-25	Christopher Brown	Affichage des pays
2017-04-26	Christopher Brown	affichage des villes, affichage des stations
2017-04-27	Christopher	Brown	affichage interdépendant, follow_station()
=========================================================
****************************************/

//Envoit en forme de select tous les pays dans la base de données
$.getJSON( '/API/getStationCountry', function( data ) {
		if(data["state"] === "Success")
		{	
			
			var array = data["country"];
			var array2 = data["id_country"];

			var selectList = document.createElement("select");
			selectList.id = "country_select";
			selectList.className = "form-control";
			selectList.size = "4";

			for (var i = 0; i < array.length; i++) {
			var option = document.createElement("option");
				option.value = array2[i];
				option.text = array[i];
				selectList.appendChild(option);
			}
			
			var myDiv = document.getElementById("country");
			myDiv.appendChild(selectList);
			
			//selectList.setAttribute("onchange", changeCity(selectList.value));
			//selectList.onchange = "changeCity(selectList.value)";
			selectList.addEventListener("change", function() {changeCity(selectList.value)}, false); 
			
			var city = document.getElementById("city");
			city.addEventListener("change", function() {changeStation(city.value)}, false);
			
		} else {
			//Request fail
			console.log("Failed to get countrys");
		}
			
});

//Envoit en forme de select les villes selon le pays sélectionné
function changeCity(selected) {
	$.getJSON( '/API/getStationCity?id='+selected, function( data ) {
			if(data["state"] === "Success")
			{	
		
				var array = data["city"];
				var array2 = data["id_city"];

				/*var selectList = document.createElement("select");
				selectList.id = "city_select";
				selectList.className = "form-control";*/
				/*var option = document.createElement("option");
				option.value = "";
				option.text = "--City--";*/
				//selectList.appendChild(option);
				/*for (var i = 0; i < array.length; i++) {
				var option = document.createElement("option");
					option.value = array[i];
					option.text = array[i];
					selectList.appendChild(option);
				}
				var myDiv = document.getElementById("city");
				myDiv.appendChild(selectList);*/
				
				
				var city = document.getElementById("city");
				city.options.length=0
				var station = document.getElementById("station");
				station.options.length=0
				
				for (var i = 0; i < array.length; i++) {
					var option = new Option(array[i], array2[i]);
					city.options.add(option);
				}
				
			} else {
				//Request fail
				console.log("Failed to get citys");
			}	
	});
}

//Envoit en forme de select les villes selon le pays sélectionné
function changeStation(value) {
	$.getJSON( '/API/getStations?id='+value, function( data ) {
		if(data["state"] === "Success")
		{	
			
			var array = data["station"];
			var array2 = data["id_station"];

			/*var selectList = document.createElement("select");
			selectList.id = "station_select";
			selectList.className = "form-control";
			for (var i = 0; i < array.length; i++) {
			var option = document.createElement("option");
				option.value = id[i];
				option.text = array[i];
				selectList.appendChild(option);
			}
			var myDiv = document.getElementById("station");
			myDiv.appendChild(selectList);*/
			
			var station = document.getElementById("station");

			station.options.length=0
			for (var i = 0; i < array.length; i++) {
				var option = new Option(array[i], array2[i]);
				station.options.add(option);
			}
			
		} else {
			//Request fail
			console.log("Failed to get stations");
		}
					
	});	
}
//Permet d'enregistrer une nouvelle station à suivre
function followStation() {
	var station_id = document.getElementById("station").value;
	$.getJSON( '/API/getStations?ua_id='+ua_id+'station_id='+station_id, function( data ) {
		if(data["state"] === "Success")
		{	
			var station = document.getElementById("station").text;
			alert("Following station : " + station);
			
			
		} else {
			//Request fail
			alert("Error!");
			console.log("Failed to get stations");
		}
					
	});
}
