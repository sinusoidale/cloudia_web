
// Fichier: notificationCircle.js
// Auteur: Guillaume Bergs
// Fonctionnalit�: W041 Syst�me d�alerte 
// Date: 2017-05-18
// V�rification: Date               Nom       
// =========================================================
// Historique de modifications:
// Date               Nom                   Description
// =========================================================
// 
//

//var connection = require('../../API/getInfo.js');
var nbAlertes = 0;

$.getJSON( '/API/getNbAlerts?id=11', function( data ) {

	//Api request is a success or not
	if(data["state"] === "Success")
	{
		//Request Succes

		nbAlertes = data["nb"];

		if(nbAlertes > 0 ){	
			var div = document.createElement('div');
			div.classList.add('notificationCircle');
			var a = document.createElement('a');
					
			a.appendChild(document.createTextNode(nbAlertes));
			div.appendChild(a);

			var source = document.getElementById('notificationCircle');
			source.parentNode.insertBefore(div, source);
		}
	}
	else
	{
		//Request fail
		console.log("Fail to load");
	}		
});
