//YOUTUBE --> IT RAINGING TACOS


var sations;
var station;
var sensors_units;
var stationData;
var stationType;
var stationUnit;
var myLineChart = null;
var stationLastUpdate;
var about_data;
var graph;
var labels = [];
var updateGraph = false;
var DataAdd = [[]];
var currentStationIndex;
var alertData;
var lastSensorIndex = -1;
var dividedDataAverage = [[[]]];                //Tableau d'average de données sur un periode

var text;

var stationList;

var text;

//Array of month in nombre
var numberToMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
/* Fonctions qui permet d'afficher ou non les élments*/


/*Fonction qui rajoute les sation*/
function show_station(){

    loadLanguageJson();

    window.onload = function(){

        loadLanguageJson();
        //console.log("Dans la fonction show_station: WAIT TO GET JSON");
        station = document.getElementById("station");
        stationList = document.getElementById("white-stationList");

        $.getJSON( '/API/getAvailableStation', function( data ) {

            //Api request is a success or not
            if(data["state"] === "Success")
            {
                //Request Succes

                //Get the Json for station
                stationType = data["station"];

            }
            else
            {
                //Request fail
                console.log("Fail to load Station");
            }

            if(stationType.length > 0) {
                display_station_unit(0);
            }

            stationList.innerHTML = elements_to_row_html(stationType);

        });



        //Load les paramètres de la du chart      
        google.charts.load('current', {'packages':['annotationchart']});
        
    }

    //  Je suis Benoit le perdu du Web. Julie est vraiment la meilleur c'es mon idole !!!
};

function csvExport() {
    exportToCSV(currentStationIndex);
}

/*C'EST ici que on doit changer pour le menu déroulant*/
function elements_to_row_html(stationType){

    var innerHTML = '';

    //TODO Le style du menu
    //TOOD documentation des class 

    //  Ajoute les stations disponible dans la barre de nav en haut
    innerHTML += '<img class="dropdown-toggle" data-toggle="dropdown" role="button" id="nav-icon-white-station" src="assets/pictograms/white_station.png" />';
    innerHTML += '<ul class="dropdown-menu" role="menu" aria-labelledby="accountDrop">';

    //  Parcours tout les stations
    for (var index = 0; index < stationType.length; ++index) {
        innerHTML +=    '<li class="stationList"><a href="#" onclick=display_station_unit("'+index+'")>' +stationType[index]["name"] + '</a></li>';
        innerHTML +=    '<li class="divider"></li>';
    }

    //  TODO: Traduction
    innerHTML +=    '<li class="stationList" id="bold"><a href="#">Ajouter une station</a></li>';
    innerHTML += '</ul>';

    return innerHTML;
};

//Fonction who show the Title of the station and the last update time
//SHOW -- > Name for the station in title
//SHOW --> Date last update data
//SHOW --> Image of the stations
function display_station_status(index) {
    console.log(stationType[index]);
    //Get last update
    $.getJSON( '/API/getLastUpdate?id='+stationType[index]["id"], function( data ) {
        //Api request is a success or not
        if(data["state"] === "Success")
        {
            //Request Succes
            //Get the Json for station
            stationLastUpdate = data["dateTime"].toString();

        }
        else
        {
            //Request fail
            console.log("Fail to load Station");
            stationLastUpdate = "Have a tea, the date is not available."
        }

        stationList.innerHTML = elements_to_row_html(stationType);

        var title_station =  document.getElementById("station_titre_area");

        var innerHTML = '';
        innerHTML += '<div id="stationInfoArea" class="col-xs-12">';
        innerHTML += '<div id="station_info" class="col-xs-5">';

        innerHTML += '<img class="img-responsive" id="stationImage" src="assets/station/station'+stationType[index]["type"]+'.png">';
        innerHTML += '<div id="station_three_info">'
        innerHTML += '<h2 id="stationName" class="orange-header">'+stationType[index]["name"]+'</h2>';
        innerHTML += '<h6 id="stationDescription">'+stationType[index]["description"]+'</h6>';
        innerHTML += '<h7 id="stationLocation">'+stationType[index]["location"]+'</h7>';
        innerHTML += '</div>'

        //  Bouton dropdown
        innerHTML += '<div class="dropdown" id="stationInfoBarDropDown">';
        innerHTML += '<button class="dropdown dropdownButtonStationInfo" id="station-drop" type="button" data-toggle="dropdown">';
        innerHTML += '<span class="caret"></span></button>';
        innerHTML += '<ul class="dropdown-menu">';

        //  Todo: Traduction
        innerHTML += '<li><a href="#">'+'Ajouter un capteur'+'</a></li>';
        innerHTML += '<li><a href="#">'+'Modifier'+'</a></li>';
        innerHTML += '<li><a href="#">'+'Supprimer'+'</a></li>';
        innerHTML += '</ul>';
        innerHTML += '</div></div>';

        //  Ajoute la zone d'info
        innerHTML += '<div id="infoStationInfo" class="col-xs-6">';
        innerHTML += '<h5>'+stationType[index]["model"]+'</h6>';
        innerHTML += '<h6>'+stationType[index]["brand"]+'</h6>';

        //  TODO: Ajouter traduction pour label "Installé le" qui a été enlevé. Label nécessaire pour mettre en contexte la date.
        innerHTML += '<h7 class="green-header">' + '2015/06/22'+'</h7>';
        innerHTML += '</div>';

        //  Ajoute la map
        innerHTML += '<div id="map" class="col-xs-3">';
        innerHTML += '</div>';

        innerHTML += '</div>';


        //  Ajoute le script qui load google map
        var headContent = document.getElementsByTagName('head')[0];
        var script = document.createElement("script");
        script.setAttributeNode(document.createAttribute("async"));
        script.setAttributeNode(document.createAttribute("defer"));
        var src = document.createAttribute("src");
        src.value = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDXHidUIR7230tTlBA0KKWUs1OLeubezEk&callback=initMap";
        script.setAttributeNode(src);
        headContent.appendChild(script);

        title_station.innerHTML = innerHTML;

    });
}

//Fonction that shows the unit of the station in arguments
//SHOW --> Unite
//SHOW --> Image
//SHOW --> Différence entre la dernière valeur
//SHOW --> Flèche de montage de donnée ou dessente
function display_station_unit(index) {
    lastSensorIndex = -1;
    //Display de title
    display_station_status(index); //TODO Change pour le load une fois
    //HELLO FROM THIS OTHER SIDE (Adelle 2016, Album : Hello)
    //Load Unit for the station
    $.getJSON( '/API/getStationData?id='+stationType[index]["id"]+'&withStatus=0', function( data ) {


        //Api request is a success or not
        if(data["state"] === "Success")
        {
            //Request Succes

            //Get the Json for station
            stationData = data["data"];
        }
        else
        {
            //Request fail
            console.log("Fail to load Station");
        }


        var innerHTML = '';
        //Show the image
        for(var i = 0; i < stationType[index]["sensor"].length; i++)
        {
            var lastdata = "";
            var difference = 0;
            var fleche = 0;
            if(i < stationData.length){

                //Get the most resente data
                lastdata = stationData[i]["sensorData"][0]["value"];

                //Calcule de différence avec la dernière donner
                difference = parseInt(stationData[i]["sensorData"][0]["value"]) - parseInt(stationData[i]["sensorData"][1]["value"]);

                //Détermine si on met un flèche vers le haut, vers le bas ou un ligne ou un X is c'est indéfinie
                fleche = 3;
                if(difference === 0) fleche = 0;
                else if(difference > 0) fleche = 1;
                else if(difference < 0) fleche = 2;
            }

            //Ici pour afficher les onglets data
            innerHTML  +='<div class="active lst_sensor" id="sensor' + i + '" onclick="switchCapteur('+i+')">';
            /*******************Version test antoine*******************/
            //innerHTML  +='<nav class="active scrollable" class="capteur_menu"  onclick="display_alert('+i+')">';
            /**********************************************************/
            //innerHTML  +='<div class="active scrollable"  onclick="display_table('+i+')">';
            innerHTML  +='<div class="img-lst"><img src="assets/mesure/mesures'+stationType[index]["measure_type"][i]+'.png" class="iconzone img-responsive"></div>';

            innerHTML  +='<div class="infozone">';
            innerHTML  +='<div id="lst-left"  class="blue-header">';
            innerHTML  +='<h4>'+stationType[index]["description_unit"][i]+'</h4>';
            innerHTML  +='</div><div id="lst-right" >';
            if(lastdata === ""){
                innerHTML  +='<h4>'+lastdata + '</h4>';
            }
            else{
                var decimal = Math.floor((lastdata % 1)*100);
                var entier = Math.floor(lastdata);
                innerHTML  +='<div class="mesure"><div class="entier">'+entier + '</div><div class="decimal">.' + decimal + '</div> <div class="unite">' +stationType[index]["sensor_name"][i]+ '</div>';
            }
            innerHTML  +='</div>';

            innerHTML  +='</div>';


            innerHTML += '</div>';

            innerHTML += '<div class="dropdown capteurInfoBarDropDown">';
            innerHTML += '<button class="dropdown dropdownButtonStationInfo" type="button" data-toggle="dropdown">';
            innerHTML += '<span class="caret"></span></button>';
            innerHTML += '<ul class="dropdown-menu">';

            //  Todo: Traduction
            innerHTML += '<li><a href="#">'+'Desactiver'+'</a></li>';
            innerHTML += '<li><a href="#">'+'Modifier'+'</a></li>';
            innerHTML += '<li><a href="#">'+'Supprimer'+'</a></li>';

            console.log("Index : " + stationData[index]);
            if (typeof stationType[index] != 'undefined') {
                var id = stationType[index]["id"];   

                if (typeof stationData[index] != 'undefined') {
                    var data = stationData[index]["measure_type"];
                    //                    innerHTML += '<li><a href="' + "http://www.cloudiaproject.org/dataCSV/" + id + "_" + data + '">'+'Exporter les données'+'</a></li>';    
                }
            }

            innerHTML += '</ul>';
            innerHTML += '</div>';
            innerHTML  +='</div>';

            innerHTML += '</nav>';
        }

        //Add the image for unit
        var element = document.getElementById("station_unite_area");
        element.innerHTML = innerHTML;

        currentStationIndex = index;

        display_table(0);
    });

}

//Divise les données selon une periode (en heure)
function divide_data_by_period(periode){

    dividedDataAverage = [];
    for(var iUnit = 0; iUnit < stationData.length; iUnit++){

        dividedDataAverage[iUnit] = [];
        var firstDataDate = new Date(stationData[iUnit]["sensorData"][0]["timestamp"]);

        var allData = [];
        var dataTotal = 0;
        var nbData = 0;
        var index = 0;
        var maxData = null;
        var minData = null;
        var standartDeviation = null;

        for(var iData = 0; iData <stationData[iUnit]["sensorData"].length; iData++){

            var dataDate = new Date(stationData[iUnit]["sensorData"][iData]["timestamp"]);
            
            
            
            allData.push(m_data);
            var timeDiff = Math.abs(dataDate.getTime() - firstDataDate.getTime());
            var diffHours = Math.floor(timeDiff / (1000 * 3600)); 
            if(diffHours < periode){
                var m_data = stationData[iUnit]["sensorData"][iData]["value"];
                
                dataTotal += parseFloat(m_data);
                
                if(!maxData | maxData < m_data){
                    maxData = m_data;
                }

                if(!minData | minData > m_data){
                    minData = m_data;
                }
                
                nbData++;
            }

            else{
                standartDeviation = standardDeviation(allData);
                dividedDataAverage[iUnit][index] = [];
                dividedDataAverage[iUnit][index]["timestamp"] = firstDataDate;
                dividedDataAverage[iUnit][index]["value"] = [];
                dividedDataAverage[iUnit][index]["value"][0] = dataTotal/nbData;
                dividedDataAverage[iUnit][index]["value"][1] = maxData;
                dividedDataAverage[iUnit][index]["value"][2] = minData;
                dividedDataAverage[iUnit][index]["value"][3] = standartDeviation;

                firstDataDate = dataDate;
                dataTotal = parseFloat(stationData[iUnit]["sensorData"][iData]["value"]);
                nbData = 1;
                index++;
                maxData = null;
                minData = null;
                allData = [];
            }
        }

        standartDeviation = standardDeviation(allData);
        
        dividedDataAverage[iUnit][index] = [];
        dividedDataAverage[iUnit][index]["timestamp"] = firstDataDate;
        dividedDataAverage[iUnit][index]["value"] = [];
        dividedDataAverage[iUnit][index]["value"][0] = dataTotal/nbData;
        dividedDataAverage[iUnit][index]["value"][1] = maxData;
        dividedDataAverage[iUnit][index]["value"][2] = minData;
        dividedDataAverage[iUnit][index]["value"][3] = standartDeviation;
    }
    
}

function standardDeviation(arr){
    var nbTable = arr.length;
    
    var ec = 0;
    var x_bar = 0;
    var i;
    for(i = 0; i< nbTable; i++){
        x_bar += i;
    }
    
    x_bar = x_bar/nbTable;
    
    for(i = 0; i < nbTable; i++){
        ec += (i-x_bar) * (i - x_bar);
    }
    
    ec = Math.sqrt((ec/nbTable));
    
    return ec;
}


//Function that shows the table
//SHOW --> table
function display_table(clickvalue) {

    divide_data_by_period(8);
    currentStationIndex = clickvalue;
    var year ="";
    var month="";
    var day  ="";
    var nb = stationData[clickvalue]["sensorData"].length;
    //Request Succes
    var day = shortDate(stationLastUpdate);

    //afficher div_menu
    var innerHTML = '<div class="div_menu" style="width=40%"></div>';

    //Afficher le début du tableau

    innerHTML='<div style="height:70%; overflow-y:auto;">';
    innerHTML += '<table id="table_data">';

    innerHTML += '<tr>';
    innerHTML += '<th class="table_date">Date</th>';
    innerHTML += '<th>Time</th>';
    innerHTML += '<th style="text-align:right">Average</th>';
    innerHTML += '<th style="text-align:right">Max</th>';
    innerHTML += '<th style="text-align:right">Min</th>';
    innerHTML += '<th style="text-align:right">Standart deviation</th>';
    innerHTML += '</tr>';
    for(var i = 0; i < dividedDataAverage[clickvalue].length; i++){
        var time = dividedDataAverage[clickvalue][i]["timestamp"].toString();

        //date
        var date = shortDate(time);

        time = time.split(" ");
        if(i===0) day = time[2];
        //heure
        var hour = time[4];

        innerHTML += '<tr>';
        innerHTML += '<td class="table_date">'+date+'</td>';
        innerHTML += '<td>'+hour+'</td>';
        
        for(var ji = 0; ji < 4; ji++){
            
            //si leur est entre deux plages 
            var num= dividedDataAverage[clickvalue][i]["value"][ji];
            var n = Math.round(num*100)/100;
            var j = n.toFixed(2);


            innerHTML += '<td style="text-align:right">'+j+'</td>';
        }
        
        innerHTML += '</tr>';
    }

    innerHTML += '</table>';
    innerHTML += '</div>';

    var element = document.getElementById("data");
    element.innerHTML = innerHTML;
                
    var csvButton = document.getElementById("csvButton");
    var onClick = document.createAttribute("onclick");
    onClick.value = "csvExport()";
    csvButton.setAttributeNode(onClick);
}

//Fonction to show option panel
function display_option(){

    //menu Recherche
    var innerHTML = '<h4 class="option_title" >Recherche</h4>';
    innerHTML += '<div>';
    innerHTML += '<select class="date_select" id="date_from">';
    innerHTML += '</select>';
    innerHTML += '<select class="date_select" id="date_to">';
    innerHTML += '</select>';
    innerHTML += '</div>';

    //menu Période
    innerHTML += '<h4 class="option_title" >Période</h4>';
    innerHTML += '<div id="period_list">';
    innerHTML += '<div class="period_list">';
    innerHTML += '<select class="period_select" id="date_from">';
    innerHTML += '</select>';
    innerHTML += '<select class="period_select" id="date_to">';
    innerHTML += '</select>';
    innerHTML += '<img'
    innerHTML += '</div></div>'
    innerHTML += '</div>';
    innerHTML += '</div>';

    //menu Afficher
    innerHTML += '<h4 class="option_title">Afficher</h4>';
    innerHTML += '<div style="padding-left:5%; padding-bottom:5%;">';

    innerHTML += '<div style="display:inline-block;">';
    innerHTML += '<input type="checkbox" name="stats" value="average">';
    innerHTML += 'Average</br>';
    innerHTML += '</input>';
    innerHTML += '<input type="checkbox" name="stats" value="deviation">';
    innerHTML += 'Standard deviation';
    innerHTML += '</input>';
    innerHTML += '</div>';

    innerHTML += '<div style="display:inline-block; padding-left:5%;">';
    innerHTML += '<input type="checkbox" name="stats" value="min">';
    innerHTML += 'Min </br>';
    innerHTML += '</input>';
    innerHTML += '<input type="checkbox" name="stats" value="max">';
    innerHTML += 'Max';
    innerHTML += '</input>';
    innerHTML += '</div>';

    innerHTML += '</div>';
    innerHTML += '<a class="option_title">Export to CSV</a>';
    var element = document.getElementById("option");
    element.innerHTML = innerHTML;
}

//Fonction to get the date
function shortDate(time){
    var time2="";

    time = time.split(" ");
    for(var i=1; i<4; i++){
        time2+=time[i]+" ";
    }
    return time2;
}

function shortDateHour(time){
    var time2="";

    time = time.split(" ");
    for(var i=1; i<5; i++){
        time2+=time[i]+" ";
    }
    return time2;
}


//Fonction who show the graph
//SHOW --> Graph
//https://developers.google.com/chart/interactive/docs/gallery/annotationchart
function display_station_graph(clickvalue) {

    $("#data").html("<button type='button' class='btn btn-default' id='button_tableau_mesure' onclick='display_statiton_mesure("+clickvalue+")'>Voir tableau des mesures</button><div id='chart_div' style='width: 100%;height:450px;'></div><button type='button' class='btn btn-default' id='button_plus_donner' onclick='more_data("+clickvalue+")''>Load les 200 données précédentes</button>");

    graph = new google.visualization.DataTable();

    //Add notation
    graph.addColumn('date','Date');
    graph.addColumn('number',stationData[clickvalue]["name"]);
    graph.addColumn('string','Mersure');

    //TODO Voir les limitations que on veut mettre pour les données du graphique
    if(!updateGraph)
    {
        //reset Data on the graph
        DataAdd = [[]];

        if(stationData[clickvalue]["sensorData"].length < 200)
        {
            for(var i = 0;i < stationData[clickvalue]["sensorData"].length; i++){

                //Get Date
                var date = stationData[clickvalue]["sensorData"][i]["timestamp"];
                var string = [];
                string = date.split(" ");
                //Get Jour
                var jour = parseInt(string[2]);
                //Function qui permet d'avoir le mois
                //TODO Faire cette démarche sur le serveur
                var mois = string[1];
                var j = 0;
                while(numberToMonth[j] === mois)
                {   
                    if(j === 11)
                    {
                        break;
                    }

                    j++; 
                }
                //Mois en nombre
                mois = j;

                //Get Year
                var year = parseInt(string[3]);
                var donnee = parseInt(stationData[clickvalue]["sensorData"][i]["value"]);
                var time = string[4].split(":");
                var hour = parseInt(time[0]);
                var minute = parseInt(time[1]);
                var seconde = parseInt(time[2]);

                DataAdd[i] = [new Date(year,mois,jour,hour,minute,seconde),donnee,stationData[clickvalue]["sensorData"][i]["value"]];
            } 
        }else{
            for(var i = 0; i < 200; i++){

                //Get Date
                var date = stationData[clickvalue]["sensorData"][i]["timestamp"];
                var string = [];
                string = date.split(" ");
                //Get Jour
                var jour = parseInt(string[2]);
                //Function qui permet d'avoir le moi
                //TODO Faire cette démarche sur le serveur
                var mois = string[1];
                var j = 0;
                while(numberToMonth[j] === mois)
                {   
                    if(j === 11)
                    {
                        break;
                    }

                    j++; 
                }
                //Mois en nombre
                mois = j;

                //Get Year
                var year = parseInt(string[3]);
                var donnee = parseInt(stationData[clickvalue]["sensorData"][i]["value"]);
                var time = string[4].split(":");
                var hour = parseInt(time[0]);
                var minute = parseInt(time[1]);
                var seconde = parseInt(time[2]);

                DataAdd[i] = [new Date(year,mois,jour,hour,minute,seconde),donnee,stationData[clickvalue]["sensorData"][i]["value"]];
            } 
        }
    }else{
        var NbDonnéeActuelle = DataAdd.length;

        if(NbDonnéeActuelle + 200 < stationData[clickvalue]["sensorData"].length)
        {
            for(var i = NbDonnéeActuelle; i < 200 + NbDonnéeActuelle; i++){

                //Get Date
                var date = stationData[clickvalue]["sensorData"][i]["timestamp"];
                var string = [];
                string = date.split(" ");
                //Get Jour
                var jour = parseInt(string[2]);
                //Function qui permet d'avoir le mois
                //TODO Faire cette démarche sur le serveur
                var mois = string[1];
                var j = 0;
                while(numberToMonth[j] === mois)
                {   
                    if(j === 11)
                    {
                        break;
                    }

                    j++; 
                }
                //Mois en nombre
                mois = j;

                //Get Year
                var year = parseInt(string[3]);
                var donnee = parseInt(stationData[clickvalue]["sensorData"][i]["value"]);
                var time = string[4].split(":");
                var hour = parseInt(time[0]);
                var minute = parseInt(time[1]);
                var seconde = parseInt(time[2]);

                DataAdd[i] = [new Date(year,mois,jour,hour,minute,seconde),donnee,stationData[clickvalue]["sensorData"][i]["value"]];
            }
        }
        else{
            for(var i = NbDonnéeActuelle; i < stationData[clickvalue]["sensorData"].length; i++){

                //Get Date
                var date = stationData[clickvalue]["sensorData"][i]["timestamp"];
                var string = [];
                string = date.split(" ");
                //Get Jour
                var jour = parseInt(string[2]);
                //Function qui permet d'avoir le moi
                //TODO Faire cette démarche sur le serveur
                var mois = string[1];
                var j = 0;
                while(numberToMonth[j] === mois)
                {   
                    if(j === 11)
                    {
                        break;
                    }

                    j++; 
                }
                //Mois en nombre
                mois = j;

                //Get Year
                var year = parseInt(string[3]);
                var donnee = parseInt(stationData[clickvalue]["sensorData"][i]["value"]);
                var time = string[4].split(":");
                var hour = parseInt(time[0]);
                var minute = parseInt(time[1]);
                var seconde = parseInt(time[2]);

                DataAdd[i] = [new Date(year,mois,jour,hour,minute,seconde),donnee,stationData[clickvalue]["sensorData"][i]["value"]];
            }
            //Change le texte du button
            $("#button_plus_donner").html("Plus rien à afficher");
        } 
    }

    //JUST DO IT :)
    console.log(DataAdd);
    graph.addRows(DataAdd);
    google.charts.setOnLoadCallback(drawChart);

    updateGraph = false;

}

function drawChart() {

    //var chart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));

    var options = {
        displayAnnotations: true
    };

    //chart.draw(graph, options);
}

//Show 200 more data on the graph
function more_data(clickvalue)
{
    updateGraph = true;

    display_station_graph(clickvalue)
}


//Fonction who show the table
//SHOW --> The table
//Tableau utiliser  -- >https://datatables.net/examples/basic_init/scroll_y.html
function display_statiton_mesure(clickvalue) {

    $("#data").html("<button type='button' class='btn btn-default' id='button_tableau_mesure' onclick='display_station_graph("+clickvalue+")'>Voir le graphique</button><div id='data_table' style='width: 100%;height:450px;'></div><button type='button' class='btn btn-default' id='button_plus_donner' onclick='download_data("+clickvalue+")''>Télécharger le graphique</button>");

    var innerHTML = '<table style="width:100%" id="table" class="display" cllspacing="0">';
    innerHTML += '<thead>';
    innerHTML += '  <tr>';
    innerHTML += '      <th>Date</th>';
    innerHTML += '      <th>'+stationData[clickvalue]["unit"]+'</th>';
    innerHTML += '  </tr>';

    innerHTML += '<tbody>';

    for(var i = 0; i < stationData[clickvalue]["sensorData"].length; i++)
    {
        innerHTML += '<tr>';
        innerHTML += '  <td>' + stationData[clickvalue]["sensorData"][i]["timestamp"] + '</td>';
        innerHTML += '  <td>' + stationData[clickvalue]["sensorData"][i]["value"] + '</td>';
        innerHTML += '</tr>';
    }

    innerHTML += '</tbody>';
    innerHTML += '</table>';

    //Add the table
    var element = document.getElementById("data_table");
    element.innerHTML = innerHTML;

    $('#table').DataTable( {
        "scrollY":        "23em",
        "scrollCollapse": true,
        "paging":         true
    } );

}

function getAboutStationData(id) {

    var dataToReturn;

    //Load About
    $.getJSON( '/API/aboutStation?id='+id, function( data ) {

        //Api request is a success or not
        if(data["state"] === "Success")
        {
            //Request Succes

            //Get the Json for station
            dataToReturn = data;
            console.log(dataToReturn);
            return dataToReturn;
        }
        else
        {
            //Request fail
            console.log("Fail to load Station");
        }

    });
}

//Fonction who show the about of the station in argument
//SHOW -- > Battery
//SHOW --> Status
//SHOW --> MAP
function display_station_about(index) {
    //Display de title
    display_station_status(index); //TODO Change pour le load une fois

    //Load About
    $.getJSON( '/API/aboutStation?id='+stationType[index]["id"], function( data ) {

        //Api request is a success or not
        if(data["state"] === "Success")
        {
            //Request Succes

            //Get the Json for station
            about_data = data;
        }
        else
        {
            //Request fail
            console.log("Fail to load Station");
        }

        var innerHTML = '<table style="width:100%" class="table">';

        //Show Statuts
        innerHTML  +='<tr>';
        innerHTML  +='<td class="iconzone">';
        innerHTML  +='<img src="assets/about/powerbutton.png" class="icon img-responsive">';
        innerHTML  +='</td><td>';
        if(stationType[index]["active"] == "1") innerHTML  +='<h3><div style="color:green">ON</div>/OFF</h3>';
        else if(stationType[index]["active"] == "0") innerHTML  +='<h3>ON/<div style="color:red">OFF</div></h3>';
        else innerHTML += 'Can not get station status.';    //TODO Faire la traduction
        innerHTML  +='</td>';
        innerHTML  +='</tr>';

        //Show Batterie
        innerHTML  +='<tr>';
        innerHTML  +='<td class="iconzone">';
        //innerHTML  +='<div style="font-size:3em color:#003380" class="glyphicon glyphicon-off" aria-hidden="true"></div>';
        innerHTML  +='<img src="assets/about/battery.png" class="icon img-responsive">';
        innerHTML  +='</td><td>';
        innerHTML  +='<h3>Battery</h3>';
        innerHTML  +='<h5>'+about_data["battery_percent"]+'</h5>'; 
        innerHTML  +='  </td>';
        innerHTML  +='<div>';

        innerHTML += "</table>"

        //Add the image for unit
        var element = document.getElementById("station_unite_area");
        element.innerHTML = innerHTML;


        //Add the map
        //For info see  : https://developers.google.com/maps/documentation/javascript/?hl=fr
        //Key API : AIzaSyDXHidUIR7230tTlBA0KKWUs1OLeubezEk 


        //Add Description of the balise
        var description =  about_data["model"];
        var lieu = about_data["location"];

        var dataDescriptionHTML = '<h4>'+description+'</h4>';
        dataDescriptionHTML += '<h5>'+lieu+'</h5>';

        $("#data_description").html(dataDescriptionHTML);

    });

}

//Load the map
function initMap() {

    //  var lat = parseFloat(about_data["latitude"]);
    //  var lng = parseFloat(about_data["longitude"]);

    var lat = 0;
    var lng = 0;

    if(isNaN(lat) || isNaN(lng))
    {
        getLocation(about_data["location"]);
    }

    var myLatLng = {lat: lat, lng: lng};

    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        scrollwheel: false,
        zoom: 2
    });

    map.addListener('click', function() {
        window.open("https://www.google.com/maps?q=24.197611,120.780512", '_blank');
    });

    // Create a marker and set its position.
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        title: about_data["location"]
    });

}


//Function ou take a adresse and give the latitude et longitude
//Exemple : http://www.aspsnippets.com/demos/394/
//Get the lat and long form google api
function getLocation(address)
{
    console.log(address);
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();

            //Set the map whit the good position
            about_data["latitude"] = latitude;
            about_data["longitude"] = longitude;
        } else {
            console.log("Request failed.");
        }
    });

}


function display_station_preference(station) {

    //Display de title
    display_station_status(station); //TODO Change pour le load une fois


    var innerHTML = '<table class="table-hover">';

    //Show the limite preference
    innerHTML  +='<tr class="active scrollable"  onclick="display_station_limite('+station+')">';
    innerHTML  +='  <td class="iconzone">';
    innerHTML  +='    <div style="font-size:3em" class="glyphicon glyphicon-alert" aria-hidden="true"></div>';
    innerHTML  +='  </td><td>';
    innerHTML  +='    <h4>Alerte du seuil limite</h4>';  //TODO À Traduire
    innerHTML  +='  </td>';
    innerHTML  +='</tr>';

    //Show station paramètre
    innerHTML  +='<tr class="active scrollable"  onclick="display_station_parametre('+station+')">';
    innerHTML  +='  <td class="iconzone">';
    innerHTML  +='    <div style="font-size:3em" class="glyphicon glyphicon-cog" aria-hidden="true"></div>';
    innerHTML  +='  </td><td>';
    innerHTML  +='    <h4>Paramètres de la station</h4>';  //TODO À Traduire
    innerHTML  +='  </td>';
    innerHTML  +='</tr>';

    //Show station autorisation
    innerHTML  +='<tr class="active scrollable"  onclick="display_station_permission('+station+')">';
    innerHTML  +='  <td class="iconzone">';
    innerHTML  +='    <div style="font-size:3em" class="glyphicon glyphicon-user" aria-hidden="true"></div>';
    innerHTML  +='  </td><td>';
    innerHTML  +='    <h4>Gestion des autorisations</h4>';  //TODO À Traduire
    innerHTML  +='  </td>';
    innerHTML  +='</tr>';

    innerHTML += "</table>"

    //Add the image for unit
    var element = document.getElementById("station_unite_area");
    element.innerHTML = innerHTML;

    display_station_limite(station);
}

//Gère les limites des stations
function display_station_limite(index)
{

    //Load Unit for the station
    $.getJSON( '/API/getStationData?id='+stationType[index]["id"]+'&withStatus=0', function( data ) {


        //Api request is a success or not
        if(data["state"] === "Success")
        {
            //Request Succes

            //Get the Json for station
            stationData = data["data"];
        }
        else
        {
            //Request fail
            console.log("Fail to load Station");
        }

        //var innerHTML = '<div class="media" style="margin:2em">';

        //Show the image
        //for(var i = 0; i < stationData.length; i++)
        //{

        /*<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="..." alt="...">
      <div class="caption">
        <h3>Thumbnail label</h3>
        <p>...</p>
        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
      </div>
    </div>
  </div>
</div>*/
        /*    innerHTML +=    '<div style="display:block">'
                innerHTML +=    '   <div class="media-left">';
                innerHTML +=    '       <img class"media-object" src="assets/mesure/mesures'+stationData[i]["measure_type"]+'.png" style="height:128px;">';
                innerHTML +=    '   </div>';
                innerHTML +=    '<div class="media-body">'
                innerHTML +=    '   <h4 class="media-heading">'+stationData[i]["description"]+'</h4>';
                innerHTML +=    '</div>';
                innerHTML +=    '</div>';

            }

            innerHTML += '</div>';*/

        /*
            <form role="form">
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control" id="email">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" id="pwd">
  </div>
  <div class="checkbox">
    <label><input type="checkbox"> Remember me</label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
            */

        var min = "Donnée bidon";   //TODO Changer quand on va l'avoir dans l'api
        var max = "Donnée pas trop bonne";  //TODO Changer quand on va l'avoir dans l'api

        var innerHTML = '<h4>Vous pouvez changer ici les valeurs maximal et minimal. Si les capteurs dépasse cette valeur, une alert vous sera envoyer par email et sur votre apparaile mobile.</h4>';//TODO Traduire
        innerHTML += '<div class="row">';

        for(var i = 0; i < stationData.length; i++)
        {

            innerHTML += '<div class="col-md-4" >'; //TODO Changer selon le nombre de donnée
            innerHTML += '  <div class"thumbnail" style="background-color: white; padding: 1em;">';    //TODO Changer le style
            innerHTML += '      <img class"image-responsive" src="assets/mesure/mesures'+stationData[i]["measure_type"]+'.png" style="height:10em">';
            innerHTML += '      <div class="caption">';
            innerHTML += '<h3>'+stationData[i]["description"]+'</h3>';

            innerHTML += '<form role="form">';

            //Affiche le minume
            innerHTML += '  <div class="form-group">';
            innerHTML += '  <label for="min">Valeur minimum</label>'; //TODO Traduction
            innerHTML += '  <input type="text" class="form-control" id="min" value="'+min+'">';
            innerHTML += '  </div>';

            //Affiche le maximue
            innerHTML += '  <div class="form-group">';
            innerHTML += '  <label for="max">Valeur maximue</label>'; //TODO Traduction
            innerHTML += '  <input type="text" class="form-control" id="max" value="'+max+'">';
            innerHTML += '  </div>';

            //Button pour changer les valeurs
            innerHTML += '<button type="submit" class="btn btn-default">Changer les valeurs</button>';


            innerHTML += '      </div>';
            innerHTML += '  </div>'
            innerHTML += '</div>'

        }

        innerHTML += '</div>'


        //Add the image for unit
        var element = document.getElementById("data");
        element.innerHTML = innerHTML;

    });
}

//Gère les paramètre qui peut être changer a distance sur la station
function display_station_parametre(index)
{
    alert("Les paramètre de la stations");
}

//Gère les permissions d'utilisation des stations par les utilisateurs
//Tableau --> http://www.redips.net/javascript/drag-and-drop-table-content/
function display_station_permission(index)
{

    var innerHTML = '<div id="userList">'
    innerHTML += '<h4>Vous pouvez gérer les utilisateurs de votre balise ici et utilisez la barre de recherche pour ajouter d’autre personne. Attention, vous ne pouvez pas modifier votre propre permission!</h4>';

    //Search
    innerHTML +=    '<form class="form-search" id="formSearch">';
    innerHTML +=    '<div class="input-group">';
    innerHTML +=    '<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span></span>';
    innerHTML +=    '<input type="text" class="form-control" placeholder="search" aria-describedby="basic-addon1">'
    innerHTML +=    '</div>';
    innerHTML +=    '</form>';

    //TODO Il me menque un api qui me permet de savoir les utilisateurs pour chaque utilisateur

    innerHTML += '<ul id="sortable1" class="droptrue col-sm-4"> Admin';
    innerHTML +=    '   <li class="ui-state-default" id="admin">Alex Morin</li>'
    innerHTML +=    '   <li class="ui-state-default">Benoit Dubreuil</li>';
    innerHTML +=    '   <li class="ui-state-default">Tristan Roy</li>';
    innerHTML +=    '</ul>';

    innerHTML +=    '<ul id="sortable2" class="droptrue col-sm-4"> User </ul>';

    innerHTML += '<ul id="sortable3" class="droptrue col-sm-4"> Visiteur';
    innerHTML +=    '   <li class="ui-state-default">Charle Murphy</li>'
    innerHTML +=    '   <li class="ui-state-default">Sam Bernard</li>';
    innerHTML +=    '   <li class="ui-state-default">Justin Brûlotte</li>';
    innerHTML +=    '</ul>';

    innerHTML +=    '</div>'

    //Add the image for unit
    var element = document.getElementById("data");
    element.innerHTML = innerHTML;

    //Gère le délacement des utilisateurs
    $(function() {

        $( "ul.droptrue" ).sortable({
            connectWith: "ul",
            cancel: "#admin",
            stop: function(event, ui) {

                //Va cherche la position de l'objet déplacer
                var index = ui.item.index()

                //Le nom de la personnes qui a été changer
                var name = ui.item.text();

                //Recherche dans chaque liste à la position
                var adminList = $("#sortable1").text();
                var userList = $("#sortable2").text();
                var visiteurList = $("#sortable3").text();

                if(adminList.search(name) != -1)
                {
                    console.log(name + " est Admin.") //TODO Faire la request a l'API
                }


                if(userList.search(name) != -1)
                {
                    console.log(name + " est User.") //TODO Faire la request a l'API
                } 


                if(visiteurList.search(name) != -1)
                {
                    console.log(name + " est Visiteur.") //TODO Faire la request a l'API
                }
            }
        });

        $( "#sortable1, #sortable2, #sortable3" ).disableSelection();


        $( "#admin" ).click(function() {
            alert( "Vous ne pouvez pas vous changer de permissions." );
        });

    });
}



//Gère le resize du graphique
//Il est alors responcive
$(window).resize(function(){
    drawChart();
});

function download_data(clickvalue)
{
    //Lien + idStation + typeMesure
    var link = "http://www.cloudiaproject.org/dataCSV/"+stationType[clickvalue]["id"]+"_"+stationData[clickvalue]["measure_type"];
    window.open(link);
}

//Fonction that shows the alerts of the sensor
function display_alert(index){
    var div = document.getElementById("data");

    $("#data").html("<div id=\"alert_list\" class=\"col-sm-3\"></div><div id=\"alert_info\" class=\"col-sm-9\"></div>"); 
    //Load Unit for the station
    $.getJSON( '/API/getRestriction?id='+stationType[currentStationIndex]["sensor"][index], function( data ) {
        //Api request is a success or not
        if(data[0]["state"] === "Success")
        {
            //Request Succes
            //Get the Json for station
            alertData = data;
        }
        else
        {
            //Request fail
            console.log("Fail to load Station");
        }

        var innerHTML = '';

        //Affiche les capteurs
        for(var i = 1; i < alertData.length; i++)
        {

            var value = alertData[i]["value"];
            var restrictionType = alertData[i]["restrictionType"];
            var mesureType = alertData[i]["mesureType"];
            var mesureDescription = alertData[i]["mesureDescription"];
            var mesureName = alertData[i]["mesureName"];


            //Ici pour afficher les onglets data
            innerHTML  +='<div class="active listhover"  onclick="display_form_alert('+i+')">';
            innerHTML  +='<img src="assets/mesure/mesures'+mesureType+'.png" class="iconzone img-responsive">';
            innerHTML  +='<div class="infozone_alerte">';
            innerHTML  +='<h4>'+restrictionType+'</h4>';
            innerHTML  +='<div id=\"value\">'+value+' '+ mesureName +'</div>';
            innerHTML  +='</div></div>';

        }
        //Add the image for unit
        var element = document.getElementById("alert_list");
        element.innerHTML = innerHTML;
    });
}

//Afiche le formulaire pour ajouter une alerte
function display_form_alert(index){

    var innerHTML = "";

    innerHTML += "<form>";

    innerHTML += "<select name=\"type\">";
    innerHTML += "<option value=\"Max\">Max</option>"; 
    innerHTML += "<option value=\"Min\">Min</option>";
    innerHTML += "<option value=\"Equal\">Equal</option>"; 
    innerHTML +=  "</select><br>";

    innerHTML +=  "Value: <input type=\"number\" name=\"value\"><br>";


    innerHTML += "Description : <input type=\"text\" name=\"desc\"><br>";

    innerHTML += "</form>";

    var element = document.getElementById("alert_info");
    element.innerHTML = innerHTML;
}

function loadLanguageJson() {

    var indexOfLanguage = document.cookie.indexOf("language");
    var newIndex = indexOfLanguage + 9;
    var indexOfComma = document.cookie.indexOf(";", newIndex);

    var language = document.cookie.substring(newIndex, indexOfComma);

    var languageCode;

    switch(language) {
        case "French":
            languageCode = "fr";
            break;
        case "Portugais":
            languageCode = "pt";
            break;
        default:
            languageCode = "en";
            break;
    }

    console.log(languageCode)

    $.getJSON( "https://dl.dropboxusercontent.com/u/58764507/locales/" + languageCode + ".json", function( data ) {
        text = data;        
        //  Crée la bare de nav
        document.getElementById("capteur_titre_area").innerHTML = createNavSensor();
    });
}

function displayProperty(index){
    
    $("#data").html("<div id=\"alert_list\" class=\"col-sm-3\"></div><div id=\"alert_info\" class=\"col-sm-9\"></div>"); 
    var div = document.getElementById("data");
    var innerHTML = "";

    innerHTML  +='<h1>Property</h1>';

    var element = document.getElementById("alert_list");
    element.innerHTML = innerHTML;
}

//Quand on sélectionne un capteur
function switchCapteur(index){
    if(index == -1){
        for(var i = 0; i < stationData.length; i++){
            document.getElementById("sensor"+i).className = "active lst_sensor";
        }
    }
    else{
        if(index != lastSensorIndex){
            document.getElementById("sensor"+index).className += " selected_sensor";

            if(lastSensorIndex != -1){
                document.getElementById("sensor"+lastSensorIndex).className = "active lst_sensor";
            }
        }
    }
    lastSensorIndex = index;
    console.log("index"+index);
    display_table(index);
    //    display_station_graph(index);
}

function switchPage(page) {
    switch(page) {
        case 0:     //  Measure
            display_station_unit(currentStationIndex);
            break;
        case 1:     //  Graphique
            display_station_graph(currentStationIndex);
            break;
        case 2:     //  Alerte
            display_alert(0);
            break;
        case 3:     //  Propriétés
            displayProperty(currentStationIndex);
            break;
    }

}

function createNavSensor() {
    var innerHTML = "";
    innerHTML += '<div id="capteur_titre_first_section" class="col-sm-3">';
    innerHTML += '<div id="capteur_titre_area_last_date" class="col-sm-8 col-xs-12">';
    innerHTML += '<p id="capteur_titre_area_last_date_title">' + text["last_measure"] + ' 24 mai 2016, 18:10</p>';
    innerHTML += '</div>';
    innerHTML += '</div>';

    innerHTML += '<div id="capteur_nav_measure" class="col-sm-1" onclick="switchPage(0)" onmouseover="hoverImageMeasure(true)" onmouseleave="hoverImageMeasure(false)">';
    innerHTML += '<div class="capteur_nav_image">';
    innerHTML += '<img id="capteur_nav_image_measure_ID" src="assets/pictograms/speedometer.png"/>';
    innerHTML += '</div>';

    innerHTML += '<div class="capteur_nav_title">';
    innerHTML += '<p>' + text["measure"] + '</p>';
    innerHTML += '</div>';
    innerHTML += '</div>';

    innerHTML += '<div id="capteur_nav_map" class="col-sm-1" onclick="switchPage(1)" onmouseover="hoverImageGraph(true)" onmouseleave="hoverImageGraph(false)">';
    innerHTML += '<div class="capteur_nav_image">';
    innerHTML += '<img id="capteur_nav_image_graph_ID" src="assets/pictograms/blue_graph.png"/>';
    innerHTML += '</div>';

    innerHTML += '<div class="capteur_nav_title">';
    innerHTML += '<p>' + text["graph"] + '</p>';
    innerHTML += '</div>';
    innerHTML += '</div>';

    innerHTML += '<div id="capteur_nav_alert" class="col-sm-1" onclick="switchPage(2)" onmouseover="hoverImageAlert(true)" onmouseleave="hoverImageAlert(false)">';
    innerHTML += '<div class="capteur_nav_image">';
    innerHTML += '<img id="capteur_nav_image_alert_ID" src="assets/pictograms/blue_bell.png"/>';
    innerHTML += '</div>';

    innerHTML += '<div class="capteur_nav_title">';
    innerHTML += '<p>' + text["alert"] + '</p>';
    innerHTML += '</div>';

    innerHTML += '</div>';

    innerHTML += '<div id="capteur_nav_property" class="col-sm-1" onclick="switchPage(3)" onmouseover="hoverImageProperty(true)" onmouseleave="hoverImageProperty(false)">';
    innerHTML += '<div class="capteur_nav_image">';
    innerHTML += '<img id="capteur_nav_image_property_ID" src="assets/pictograms/cog_resize.png"/>';
    innerHTML += '</div>';

    innerHTML += '<div class="capteur_nav_title">';
    innerHTML += '<p>' + text["property"] + '</p>';
    innerHTML += '</div>';
    innerHTML += '</div>';

    return innerHTML;
}


function exportToCSV(clickValue) {
    var dataArr = [];
    
     for(var i = 0; i < dividedDataAverage[clickValue].length; i++){
        var time = dividedDataAverage[clickValue][i]["timestamp"].toString();
        
        //date
        var date = shortDate(time);
        
        time = time.split(" ");
        if(i===0) day = time[2];
        var hour = time[4];

        //si leur est entre deux plages 
        var num= dividedDataAverage[clickValue][i]["value"];
        var n = Math.round(num*100)/100;
        var average = n.toFixed(2);

         dataArr.push(date);
         dataArr.push(hour);
         dataArr.push(average);
         dataArr.push("----");
    }
            
    var csvString = "Date;Time;Average;Ecart-Type;\n";
    for(var i = 0, l = dataArr.length; i < l; i++){
        if(i % 4 == 0 && i != 0) {
            csvString += "\n";
        }
        
        csvString += dataArr[i];
        csvString += ";";
    }

    var a = document.createElement('a');
    a.href = 'data:attachment/csv,' +  encodeURIComponent(csvString);
    a.target = '_blank';
    a.download = 'data.csv';

    document.body.appendChild(a);
    a.click();


}