/*
* Nom du fichier : user.js
* But : S'occuper de l'inscription, le login et le logout de l'utilisateur sur le site web.
*/
var https = require('https');
var fs = require('fs');
var ejs = require('ejs');
var crypto = require('crypto')
var connection = require("../DB.js");

//Utilise le test key pour faire des test local pour l'inscription
var SITE_KEY = '6LcrYBoTAAAAAKt4VF5tkUnNiQR4U9zvGzEo5LFK'
//site ley : 6LcrYBoTAAAAAKt4VF5tkUnNiQR4U9zvGzEo5LFK
//test key (local) : 6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI

var SECRET_KEY = '6LcrYBoTAAAAAKFGI5Zp9hVMFSv_ko8JsQPnwHOS'
//site key : 6LcrYBoTAAAAAKFGI5Zp9hVMFSv_ko8JsQPnwHOS
//test key (local) : 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe

/*
* Permet de faire le signup sur le site web
*/
exports.signup = function(req, res) {
    var first = req.body.FirstName
    var last = req.body.LastName
    var username = req.body.Username
    var email = req.body.Email
    var password = req.body.Password1
    var passwordCheck = req.body.Password2
    var phone = req.body.TelephoneNumber

    var captKey = req.body['g-recaptcha-response']
    verifyRecaptcha(captKey, function(success) {
    if(success) {

      if(iToken === undefined) {
        var iToken = "NO TOKEN"
      } 

      if (aToken ===  undefined) {
        var aToken = "NO TOKEN"
      }


      //Si tout les info sont validé...
      if(username !== undefined && password !== undefined && password === passwordCheck && emailCheck(email) && nameCheck(first) && nameCheck(last) && phoneCheck(phone)) {
        
        connection.query('SELECT * from cloudia.user_account WHERE username=\''+username+'\' OR email_address=\''+email+'\'LIMIT 1' , function(err, rows, fields) {


          if (!err) {
            if(rows.length > 0) { 
              res.render('sign_in.ejs', {error: "Username or email already exist please log in instead!"})
            } else {
              //Tout est valide, on enregistre l'utilisateur!!
              connection.query('INSERT INTO user_account (`id`, `username`, `password`, `first_name`, `last_name`, `email_address`, `phone_number`, `subscribe_date`, `extension`, `active`, `forget_password`) VALUES (\'NULL\', \''+username+'\', \''+hashString(password)+'\', \''+first+'\', \''+last+'\', \''+email+'\', \''+phone+'\', CURDATE(), \'member\', \'1\', \'NULL\')', function(err, rows, fields) {
              
                if (!err) {
                  connection.query('SELECT * from cloudia.user_account WHERE username=\''+username+'\' OR email_address=\''+email+'\'LIMIT 1' , function(err, rows, fields) {

                    if (!err) {
                      if (rows.length > 0) {
                        if((username === rows[0].username.toString() || email === rows[0].username.toString()) && hashString(password) === rows[0].password.toString()) {
                          
                          /*Anciennement, la page redirigeait vers show_measure.ejs. Il redirige maintenant vers le login. 
                          L'idée est d'envoyer une email à l'utlisateur qui vient d'être enregistré pour le confirmer.*/
                          res.redirect('/log_in');

                        } else {
                          res.render('sign_in.ejs', {error: "Username or email already exist please log in instead!"})
                        }
                    } else {
                      res.render('sign_in.ejs', {error: "Newly created user does not exist"})
                    }
                  }
                });
                } else {
                  res.render('sign_in.ejs', {error: "Error with DB 1"})
                }
              });
            }
          } else {
            res.render('sign_in.ejs', {error: "Error with DB 2"})
          }
        });
      } else {
        res.render('sign_in.ejs', {error: "Error with DB 3"})
      }
    } else {
      res.render('sign_in.ejs', {error: "Error seems like you're a robot"})
    }
  });
}

exports.logout = function(req, res) {
      //On détruit le cookie ici...
      res.clearCookie('username')
      res.clearCookie('group')
      res.clearCookie('forStation')

      //Détruit la session active
      req.session.destroy();
      
      //Redirige l'internaute vers la page d'accueil du site
      res.redirect('/');

}

exports.login = function(req, res) {

    var loginInfo = req.body.Pseudo_login
    var password = req.body.Password_login
    
    if(loginInfo === '' || password === '') {
        res.render('log_in.ejs', {error: ""})
        return;
    }


    connection.query('SELECT * from cloudia.user_account WHERE username=\''+loginInfo+'\' OR email_address=\''+loginInfo+'\'LIMIT 1' , function(err, rows, fields) {

    if (!err) {
      if (rows.length > 0) {
        if((loginInfo === rows[0].username.toString() || loginInfo === rows[0].email_address.toString()) && hashString(password) === rows[0].password.toString()) {
          //Login success
          //On crée le cookie ici
          res.cookie("username" , rows[0].username.toString(), {expire : new Date() + 9999});
          res.cookie("group", 0, {expire : new Date() + 9999});
          res.cookie("forStation" , "[bra001,4,5]", {expire : new Date() + 9999});

          //enregistre l'utilisateur à la session
          req.session.user = loginInfo;
          
          res.render('show_measure.ejs', {error: ""}) 
        } else {
          res.render('log_in.ejs', {error: "Erreur avec l'authentification"})
        }
      } else {
        res.render('log_in.ejs', {error: "Erreur avec l'authentification"})
      }
    } else {
      res.render('log_in.ejs', {error: "Erreur avec la BD"})
    }
  });
}

/*
* Fonction qui prends en argument une string a encrypté en sha512
* retourne le string encrypter
* A besoin du module crypto installé a l'aide de npm
*/
function hashString(pass) {
  var hash = crypto.createHash('sha512').update(pass).digest('hex')
    return hash
}

function verifyRecaptcha(key, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET_KEY + "&response=" + key, function(res) {
        var data = "";
        res.on('data', function (chunk) {
                        data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            } catch (e) {
                callback(false);
            }
        });
    });
}

/*
* Fonction qui prends en argument une string verifier
* retourne true/false, si la string possède des caracter illégaux pour un nom/prenom
*/
function nameCheck(name) {
  //var re = /[\u00C0-\u017Fa-zA-Z\-'\s]+/;
  var re = /^[a-zA-Z\u00C0-\u017F\s-']*$/
    return re.test(name) && name !== undefined;
}

/*
* Fonction qui prends en argument une string verifier
* retourne true/false, si la string possède des caractere illégaux pour un
* numro de téléphone
*/
function phoneCheck(phone) {
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

    return re.test(phone) && phone !== undefined
}

/*
* Fonction qui prends en argument une string verifier
* retourne true/false, si la string possède des caractere illégaux pour un
* email
*/
function emailCheck(email) {
  var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    return re.test(email) && email !== undefined;
}


