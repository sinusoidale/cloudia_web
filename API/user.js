var connection = require("../DB.js");
var crypto = require('crypto')

/*
* Essaie le login de l'utilisateurs
* retourne ces informations en JSON si réussi
*/
exports.login = function (req, res) {
    var username = req.body.username
    var password = req.body.password
    var email = req.body.email

    connection.query('SELECT * from cloudia.user_account WHERE username=\''+username+'\' OR email_address=\''+email+'\'LIMIT 1' , function(err, rows, fields) {

    if (!err) {
      if (rows.length > 0) {
        if((username === rows[0].username.toString() || email === rows[0].username.toString()) && hashString(password) === rows[0].password.toString()) {
          var obj = new Object();
          obj.state = 'Success'
          obj.error = ''
          obj.username = rows[0].username
          obj.firstName = rows[0].first_name
          obj.lastName = rows[0].last_name
          obj.email = rows[0].email_address
          obj.phone = rows[0].phone_number
          obj.id = rows[0].id
          res.end(JSON.stringify(obj))
          res.status(200)
        } else {
          var obj = new Object();
          obj.state = 'Failed'
          obj.error = 'Erreur mot de passe'
          res.status(404)
          res.end(JSON.stringify(obj));
        }
      } else {
        var obj = new Object();
        obj.state = 'Failed'
        obj.error = 'User does not exist'
        res.end(JSON.stringify(obj));
        res.status(404)
      }
      

    } else {
      var obj = new Object();
      obj.state = 'Failed'
      obj.error = 'Error with DB'
      res.end(JSON.stringify(obj));
      res.status(400)
    }
  });
}
/*
exports.userPermission = function (req, res) {
    var username = req.query.username
    var password = req.query.password
    var email = req.query.email

    connection.query('SELECT * from cloudia.user_account WHERE username=\''+username+'\' OR email_address=\''+email+'\'LIMIT 1' , function(err, rows, fields) {

    if (!err) {
      if (rows.length > 0) {
        if((username === rows[0].username.toString() || email === rows[0].username.toString()) && hashString(password) === rows[0].password.toString()) {
          var obj = new Object();
          obj.state = 'Success'
          obj.error = ''
          obj.username = rows[0].username
          obj.firstName = rows[0].first_name
          obj.lastName = rows[0].last_name
          obj.email = rows[0].email_address
          obj.phone = rows[0].phone_number
          obj.id = rows[0].id
          res.end(JSON.stringify(obj))
        } else {
          var obj = new Object();
          obj.state = 'Failed'
          obj.error = 'Erreur mot de passe'
          res.status(404)
          res.end(JSON.stringify(obj));
        }
      } else {
        var obj = new Object();
        obj.state = 'Failed'
        obj.error = 'User does not exist'
        res.end(JSON.stringify(obj));
        res.status(404)
      }
      

    } else {
      var obj = new Object();
      obj.state = 'Failed'
      obj.error = 'Error with DB'
      res.end(JSON.stringify(obj));
      res.status(400)
    }
  });
}
*/


exports.getUserForStation = function (req, res) {
    var station = req.query.station
    connection.query('SELECT * from cloudia.ta_ua_station WHERE station_id=\''+station+'\'' , function(err, rows, fields) {

    if (!err) {
      if (rows.length > 0) {
        var userCount = rows.length
        var obj = new Object();
        obj.state = 'Success'
        obj.error = ''
        obj.admin = []
        obj.user = []
        obj.visitor = []
        obj.noob = []
        var count = 0
        var groupList = []
        for (var i = 0; i < rows.length; i++) {
          var user_id = rows[i].ua_id
          var group = rows[i].group
          groupList[user_id] = group

          connection.query('SELECT * from cloudia.user_account WHERE id=\''+user_id+'\' LIMIT 1', function(err, rows, fields) {
              if (!err) {
                
                if (rows.length > 0) {
                    var username = rows[0].username
                    var obj2 = new Object();
                    obj2.id = rows[0].id
                    obj2.username = username
                    var g = groupList[obj2.id]
                    if(g === 0) {
                      obj.noob.push(obj2)
                    } else if (g === 1){
                      obj.admin.push(obj2)
                    } else if (g === 2){
                      obj.user.push(obj2)
                    } else if (g === 3){
                      obj.visitor.push(obj2) 
                    }
                }
              }
          }).on('end', function() {
              count++
              if(count == userCount) {
                res.end(JSON.stringify(obj));               
              }
          })
        }
      } else {
        var obj = new Object();
        obj.state = 'Failed'
        obj.error = 'No user or no station'
        res.end(JSON.stringify(obj));
        res.status(404)
      }
      

    } else {
      var obj = new Object();
      obj.state = 'Failed'
      obj.error = 'Error with DB'
      res.end(JSON.stringify(obj));
      res.status(400)
    }
  });
}


/*
* Permet l'enregistrement d'un utilisateurs a partir de l'API
* Verifie toutes les infos, pour qu'elles soient valide, crée
*   retourne les informations de l'utilisateurs lorsque celui-ci 
* a été créer, sinon ca retourne une erreur en JSON
*/
exports.signup = function (req, res) {
    var username = req.query.username
    var password = req.query.password
    var email = req.query.email
    var first = req.query.firstname
    var last  = req.query.lastname
    var phone = req.query.phone
    var iToken = req.query.iToken
    var aToken = req.query.aToken


    if(iToken === undefined) {
      iToken = "NO TOKEN"
    } 

    if (aToken ===  undefined) {
      aToken = "NO TOKEN"
    }


    //Si tout les info sont validé...
    if(username !== undefined && password !== undefined && emailCheck(email) && nameCheck(first) && nameCheck(last) && phoneCheck(phone)) {
      
      connection.query('SELECT * from cloudia.user_account WHERE username=\''+username+'\' OR email_address=\''+email+'\'LIMIT 1' , function(err, rows, fields) {


        if (!err) {
          if(rows.length > 0) { 
            var obj = new Object();
            obj.state = 'Failed'
            obj.error = 'Utilisateurs ou email existant'
            res.end(JSON.stringify(obj));
            res.status(406)
          } else {
            //Tout est valide, on enregistre l'utilisateur!!!
            //INSERT INTO user_account (`id`, `username`, `password`, `first_name`, `last_name`, `email_address`, `phone_number`, `subscribe_date`, `extension`, `active`, `forget_password`) VALUES (NULL, 'loulou784', 'Akin0720', 'Alex', 'Morin', 'alexmorin68@outlook.com', '819-620-3192', '2016-03-02', 'NULL', '1', NULL);
            connection.query('INSERT INTO user_account (`id`, `username`, `password`, `first_name`, `last_name`, `email_address`, `phone_number`, `subscribe_date`, `extension`, `active`, `forget_password`) VALUES (\'NULL\', \''+username+'\', \''+hashString(password)+'\', \''+first+'\', \''+last+'\', \''+email+'\', \''+phone+'\', CURDATE(), \'member\', \'1\', \'NULL\')', function(err, rows, fields) {
            
              if (!err) {
                connection.query('SELECT * from cloudia.user_account WHERE username=\''+username+'\' OR email_address=\''+email+'\'LIMIT 1' , function(err, rows, fields) {

                  if (!err) {
                    if (rows.length > 0) {
                      if((username === rows[0].username.toString() || email === rows[0].username.toString()) && hashString(password) === rows[0].password.toString()) {
                        var obj = new Object();
                        obj.state = 'Success'
                        obj.error = ''
                        obj.username = rows[0].username
                        obj.firstName = rows[0].first_name
                        obj.lastName = rows[0].last_name
                        obj.email = rows[0].email_address
                        obj.phone = rows[0].phone_number
                        obj.id = rows[0].id
                        res.end(JSON.stringify(obj))
                        res.status(200)
                      } else {
                        var obj = new Object();
                        obj.state = 'Failed'
                        obj.error = 'Erreur mot de passe'
                        res.end(JSON.stringify(obj));
                        res.status(404)
                      }
                  } else {
                    var obj = new Object();
                    obj.state = 'Failed'
                    obj.error = 'Newly created user does not exist'
                    res.end(JSON.stringify(obj));
                    res.status(400)
                  }
                }
              });
              } else {
                var obj = new Object();
                obj.state = 'Failed'
                obj.error = 'Error with DB 1'
                res.end(JSON.stringify(obj));
                res.status(400)
              }
            });
          }

        } else {
          var obj = new Object();
          obj.state = 'Failed'
          obj.error = 'Error with DB 2'
          res.end(JSON.stringify(obj));
          res.status(400)
        }
      });
    } else {
      var obj = new Object() 
      obj.state = "Failed";
      obj.error = "Probleme avec des info";
      obj.emailValid = emailCheck(email);
      obj.phoneValid = phoneCheck(phone);
      obj.firstnameValid = nameCheck(first);
      obj.lastnameValid = nameCheck(last);
      obj.usernameValid = username !== undefined;
      obj.passwordValid = password !== undefined;
      res.send(JSON.stringify(obj))
      res.status(406)
    }
}


/*
* Fonction qui prends en argument une string a encrypté en sha512
* retourne le string encrypter
* A besoin du module crypto installé a l'aide de npm
*/
function hashString(pass) {
  var hash = crypto.createHash('sha512').update(pass).digest('hex')
    return hash
}

/*
* Fonction qui prends en argument une string verifier
* retourne true/false, si la string possède des caracter illégaux pour un nom/prenom
*/
function nameCheck(name) {
  //var re = /[\u00C0-\u017Fa-zA-Z\-'\s]+/;
  var re = /^[a-zA-Z\u00C0-\u017F\s-']*$/
    return re.test(name) && name !== undefined;
}

/*
* Fonction qui prends en argument une string verifier
* retourne true/false, si la string possède des caractere illégaux pour un
* numro de téléphone
*/
function phoneCheck(phone) {
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    return re.test(phone) && phone !== undefined
}

/*
* Fonction qui prends en argument une string verifier
* retourne true/false, si la string possède des caractere illégaux pour un
* email
*/
function emailCheck(email) {
  var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return re.test(email) && email !== undefined;
}
