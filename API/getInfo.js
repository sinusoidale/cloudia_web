/****************************************
Fichier : index.js
Auteur :
Fonctionnalité : Configuration pour démarrer le serveur
Date : 2017-04-12

Vérification :
Date	Nom		Approuvé
=========================================================
Historique de modifications :
Date	Nom		Description
2017-04-25	Christopher Brown	getStationCountry
2017-04-26	Christopher Brown	getStationCity, getStations
2017-04-27	Christopher Brown	followStation
2017-04-30	Christopher Brown	createStation, getStationType, getStationGroup
2014-04-27 Guillaume Bergs Ajout de la mise à jour des alertes
=========================================================
à faire :
-finir createStation
****************************************/
var connection = require('../DB.js');
var fs = require('fs');
var unitArray = getUnitArray();

/*
 * Récupère les données pour une station selon l'id
 * Paramètre obligatoire: id -> L'id de la station à rechercher
 * Paramètres optionels:
 *			fromDate	-> La date du début des données voulues au format YYYY-MM-JJ
 *			fromHour	-> L'heure de début des données voulues au format HH:MM:SS, ignoré si pas de fromDate
 *           toDate    	-> La date de fin des données voulues au format YYYY-MM-JJ
 *			toHour 		-> l'heure de fin des données voules au format HH:MM:SS, ignoré si pas de toDate
 *           withStatus  -> Si on veut les infos du statut on le met a 1 (sinon 0 ou undefined)
 * Retourne dans tous les cas du JSON, parfois très long... Avec les données le mieux séparé possible,
 * Devrait avoir tout le necessaire pour un application Android/IOS, le meilleur moyen de connaître le résultat
 * est de faire une requête
 */
exports.stationData = function (req, res) {
	//Les variables de l'URL
	var id = req.query.id;
	var fromDate = req.query.fromDate;
	var toDate = req.query.toDate;
	var withStatus = req.query.withStatus;
	var fromHour = req.query.fromHour;
	var toHour = req.query.toHour;

	//avec le stationId on va dans le sensor_unit, on obtient le suXXXX id et apres ca on va dans sensor mesure et on obtient les données
	//pour le sensor unit, on recupère ensuite les données par sensor unit qu'on doit trier par date

	//On cherche les stations selon l'ID
	connection.query('SELECT * from station WHERE id=\"' + id + '\" LIMIT 1', function (err, rows, fields) {
		if (!err) {

			//On a découvert une balise a l'aide de l'id de celle-ci Incroyable
			if (rows.length > 0) {
				var station_id = rows[0].id.toString()
					var station_name = rows[0].name.toString()
					var station_description = rows[0].description.toString()
					var station_model = rows[0].model.toString()
					var station_active = rows[0].active.toString()
					var station_location = rows[0].location.toString()

					//On détermine le station_unit
					connection.query('SELECT * from sensor_unit WHERE station_id=\"' + station_id + '\" LIMIT 1', function (err, rows, fields) {
						if (!err) {
							if (rows.length > 0) {

								var stationUnit = rows[0].id

									var query = '';

								//On sort les données uniquement quand c'est entre les dates reçues, ce qui change la query MYSQL
								query = 'SELECT * from sensor_measure WHERE sensor_id LIKE \'%' + stationUnit + '%\'';

								if (fromDate !== undefined) {
									if (fromHour !== undefined) {
										//on a une date et une heure de début
										query += 'AND date_time > STR_TO_DATE(\'' + fromDate + ' ' + fromHour + '\', \'%Y-%m-%d %T\')';
									} else {
										//on a une date de début
										query += 'AND date_time > STR_TO_DATE(' + '\'' + fromDate + '\'' + ', \'%Y-%m-%d\')';
									}
								};
								if (toDate !== undefined) {
									if (toHour !== undefined) {
										//on a une date et une heure de fin
										query += 'AND date_time < STR_TO_DATE(\'' + toDate + ' ' + toHour + '\', \'%Y-%m-%d %T\')';
									} else {
										//on a une date de fin
										query += 'AND date_time < STR_TO_DATE(\'' + toDate + '\', \'%Y-%m-%d\')';
									}
								};

								/**
								query = 'SELECT * from sensor_measure WHERE sensor_id LIKE \'%'+stationUnit+'%\'';

								if(fromDate !== undefined){
								//on a une date de début
								query += 'AND date_time > STR_TO_DATE('+ '\''+fromDate+'\'' + ', \'%Y-%m-%d\')';
								}
								if(toDate !== undefined){
								//on a une date de fin
								query += 'AND date_time < STR_TO_DATE(\''+ toDate +'\', \'%Y-%m-%d\')';
								}
								 **/

								/**
								//Si on a une date de debut et de fin
								if(fromDate !== undefined && toDate !== undefined) {
								query = 'SELECT * from sensor_measure WHERE sensor_id LIKE \'%'+stationUnit+'%\' AND (date_time > STR_TO_DATE(\''+fromDate+'\', \'%Y-%m-%d\') AND date_time < STR_TO_DATE(\''+toDate+'\', \'%Y-%m-%d\'))'}
								//On a une date de début mais pas de fin
								} else if(fromDate !== undefined && toDate === undefined) {
								query = 'SELECT * from sensor_measure WHERE sensor_id LIKE \'%'+stationUnit+'%\' AND (date_time > STR_TO_DATE(\''+fromDate+'\', \'%Y-%m-%d\'))'}
								//On a une date de fin mais pas de debut
								} else if(fromDate === undefined && toDate !== undefined) {
								query = 'SELECT * from sensor_measure WHERE sensor_id LIKE \'%'+stationUnit+'%\' AND (date_time < STR_TO_DATE(\''+toDate+'\', \'%Y-%m-%d\'))'}
								//On a pas de date
								} else {
								query = 'SELECT * from sensor_measure WHERE sensor_id LIKE \'%'+stationUnit+'%\'';
								}
								 **/

								connection.query(query, function (err, rows, fields) {
									if (!err) {
										if (rows.length > 0) {
											var arr = []
											//les données voulues
											for (var i = unitArray.length - 1; i >= 0; i--) {
												arr[i] = []
											}
											var long = ''
												var lat = ''

												//On sépare les données selon leur type de données
												for (var i = rows.length - 1; i >= 0; i--) {
													var obj = new Object();
													obj.timestamp = rows[i].date_time.toString();
													//Si ce ne sont pas des données gps
													if (rows[i].measure_type.toString() != '16') {
														obj.value = rows[i].measure_value.toString();
														arr[rows[i].measure_type].push(obj)
													} else {
														long = rows[i].measure_value.toString();
														lat = rows[i - 1].measure_value.toString();
													}
												}

												//Les données totales de la station bien organisées, et uniquement selon les dates voulues
												var dataArray = []
												for (var i = unitArray.length - 1; i >= 0; i--) {
													var obj = new Object();
													obj.measure_type = i;
													if (unitArray[i] == undefined) {
														obj.unit = ''
															obj.description = ''
															obj.sensor_id = '';
													} else {
														obj.unit = unitArray[i].name
															obj.description = unitArray[i].description
															obj.sensor_id = unitArray[i].sensor_id
													}
													obj.sensorData = arr[i];
													//On affiche uniquement les données qui ont des éléments
													if (arr[i].length > 0) {
														dataArray.push(obj)
													}
												}

												//Les mêmes données de la station mais cete fois version finale avec les info de celle ci
												var objToSend = new Object();
											//On affiche les données de l'appareil
											if (withStatus !== undefined && withStatus.toString() === '1') {
												objToSend.state = 'Success'
													objToSend.error = ''
													objToSend.name = station_name
													objToSend.id = station_id
													objToSend.stationUnit = stationUnit
													objToSend.model = station_model
													objToSend.description = station_description
													objToSend.location = station_location
													objToSend.caseHum = 'Humidité'
													objToSend.caseTemp = 'Température'
													objToSend.long = long
													objToSend.lat = lat
													objToSend.data = dataArray
											} else {
												objToSend.state = 'Success'
													objToSend.error = ''
													objToSend.name = station_name
													objToSend.id = station_id
													objToSend.data = dataArray
											}

											//On envoie les informations
											var toSend = JSON.stringify(objToSend)
												res.end(toSend)

										} else {
											res.status(404);
											res.end("Aucune donnée");
										}
									} else {
										res.status(500);
										res.end("Erreur serveur");
									}
								});
							} else {
								res.status(500);
								res.end("Erreur serveur");
							}
						} else {
							var obj = new Object();
							res.status(500);
							res.end("Erreur serveur");
						}
					});

			} else {
				res.status(404);
				res.end("Erreur id non valide");
			}

		} else {
			res.status(500);
			res.end("Erreur serveur");
		}

	});
}

/*
 * envoie en json les unité disponible selon leur numéro
 * retourne les données sous forme de json
 */
exports.unit = function (req, res) {
	var arr = []
	for (var i = unitArray.length - 1; i >= 0; i--) {
		var obj = new Object()
			obj.key = i
			if (unitArray[i] == undefined) {
				obj.unit = ''
					obj.description = ''
			} else {
				obj.unit = unitArray[i].name
					obj.description = unitArray[i].description
			}
			arr.push(obj)
	}

	var obj2 = new Object()
		if (arr.length > 0) {
			obj2.state = 'Success'
				obj2.error = ''
				obj2.unit = arr
				res.end(JSON.stringify(obj2))
		} else {
			obj2.state = 'Failed'
				obj2.error = 'Aucune unité disponible'
				res.end(JSON.stringify(obj2))
		}
		unitArray = getUnitArray()
}

/*
 * retourne la derniere date de modification d'une station
 */
exports.lastUpdate = function (req, res) {
	var id = req.query.id;
	var sensor_unit = ""
		connection.query('SELECT * from sensor_unit WHERE station_id=\"' + id + '\" LIMIT 1', function (err, rows, fields) {
			if (!err) {
				if (rows.length > 0) {

					sensor_unit = rows[0].id

						connection.query('SELECT * from sensor_measure WHERE sensor_id LIKE \'%' + sensor_unit + '%\' ORDER BY date_time DESC', function (err, rows, fields) {
							if (!err) {
								if (rows.length > 0) {
									var obj = new Object();
									obj.state = 'Success'
										obj.error = ''
										obj.dateTime = rows[0].date_time.toString()
										res.end(JSON.stringify(obj));
								} else {
									var obj = new Object();
									obj.state = 'Failed'
										obj.error = 'Id does not match'
										res.end(JSON.stringify(obj));
								}
							} else {
								var obj = new Object();
								obj.state = 'Failed'
									obj.error = 'Error with DB'
									res.end(JSON.stringify(obj));
							}
						})
				} else {
					var obj = new Object();
					obj.state = 'Failed'
						obj.error = 'Id does not match'
						res.end(JSON.stringify(obj));
				}
			} else {
				var obj = new Object();
				obj.state = 'Failed'
					obj.error = 'Error with DB'
					res.end(JSON.stringify(obj));
			}
		});
}

exports.availableStation = function (req, res) {
	connection.query('SELECT * from cloudia.station', function (err, rows, fields) {
		var resultArray = [];
		var idArray = [];
		var sensoridArray = [];
		var sensorArray = [];
		var sensorMesureID = [];
		var sensorMesureType = [];
		var totalArray = [];
		var count = 0;
		var nb = 0;
		var totalCount = 0;
		var sensorMesureIdArray = [];
		var sensorMesureIdArrayTotal = [];
		var sensorMesureName = [];

		if (!err) {
			for (var i = rows.length - 1; i >= 0; i--) {
				var obj = new Object();
				obj.name = rows[i].name.toString();
				obj.id = rows[i].id.toString();
				obj.active = rows[i].active.toString();
				obj.type = rows[i].station_type_id.toString();
				obj.description = rows[i].description.toString();
				obj.location = rows[i].location.toString();
				obj.model = rows[i].model.toString();
				obj.brand = rows[i].brand.toString();

				if (rows[i].installation_date === null) {
					obj.installation_date = "--";
				} else {
					obj.installation_date = rows[i].installation_date.toString();
				}

				resultArray.push(obj);
				idArray.push(rows[i].id.toString());

				//Fait la deuxième demande
				if (resultArray.length === rows.length) {
					for (var w = 0; w < idArray.length; w++) {
						connection.query('SELECT * from sensor_unit WHERE station_id=\'' + idArray[w].toString() + '\' LIMIT 1', function (err2, rows2, fields2) {
							if (!err2) {
								if (rows2.length > 0) {
									sensoridArray.push(rows2[0].id.toString());

									// On fait la requette suivante
									if (sensoridArray.length === w) {
										for (var y = 0; y < sensoridArray.length; y++) {
											connection.query('SELECT * from sensor WHERE sensor_unit_id=\'' + sensoridArray[y].toString() + '\'', function (err3, rows3, fields3) {
												if (!err3) {
													if (rows3.length > 0) {

														for (var e = 0; e < rows3.length; e++) {
															sensorArray.push(rows3[e].id);
															sensorMesureID.push(rows3[e].sensor_type_id);
															sensorMesureIdArray.push(rows3[e].measure_type_id);
															sensorMesureIdArrayTotal.push(rows3[e].measure_type_id)

															if (sensorArray.length === rows3.length) {
																resultArray[count].sensor = sensorArray;
																resultArray[count].measure_type = sensorMesureIdArray;
																sensorArray = [];
																sensorMesureIdArray = [];
																count++;

																if (resultArray.length === count) {
																	//Pour toute les types de senser
																	for (var k = 0; k < sensorMesureID.length; k++) {
																		connection.query('SELECT * from sensor_type WHERE id=\'' + sensorMesureID[k].toString() + '\' LIMIT 1', function (err4, rows4, fields4) {

																			if (!err4) {
																				if (rows4.length > 0) {
																					sensorMesureType.push(rows4[0].name);

																					nb++;

																					if (resultArray[totalCount].sensor.length === nb) {
																						resultArray[totalCount].description_unit = sensorMesureType;

																						totalCount++;
																						nb = 0;
																						sensorMesureType = [];

																						if (totalCount === resultArray.length) {
																							totalCount = 0;
																							nb = 0;
																							for (var h = 0; h < sensorMesureIdArrayTotal.length; h++) {
																								connection.query('SELECT * from measure_type WHERE id=\'' + sensorMesureIdArrayTotal[h].toString() + '\' LIMIT 1', function (err5, rows5, fields5) {
																									if (!err5) {
																										if (rows5.length > 0) {
																											sensorMesureName.push(rows5[0].name);
																											nb++;

																											if (nb === resultArray[totalCount].sensor.length) {
																												resultArray[totalCount].sensor_name = sensorMesureName;
																												totalCount++;
																												nb = 0;
																												sensorMesureName = [];

																												if (totalCount === resultArray.length) {
																													var objk = new Object()
																														objk.state = 'Success'
																														objk.error = ''
																														objk.station = resultArray
																														res.end(JSON.stringify(objk));
																												}
																											}

																										}
																									}

																								});
																							}

																						}

																					}

																				} else {
																					var obj = new Object();
																					obj.state = 'Failed'
																						obj.error = 'Error with DB'
																						res.end(obj);
																				}
																			} else {
																				var obj = new Object();
																				obj.state = 'Failed'
																					obj.error = 'Error with DB'
																					res.end(obj);
																			}

																		});
																	}

																}

															}
														}

													} else {
														var obj = new Object();
														obj.state = 'Failed'
															obj.error = 'Error with DB'
															res.end(obj);
													}
												} else {
													var obj = new Object();
													obj.state = 'Failed'
														obj.error = 'Error with DB'
														res.end(obj);
												}
											});
										}

									}

								} else {
									var obj = new Object();
									obj.state = 'Failed'
										obj.error = 'Error with DB'
										res.end(obj);
								}

							} else {
								var obj = new Object();
								obj.state = 'Failed'
									obj.error = 'Error with DB'
									res.end(obj);
							}

						});

					}
				}
			}

		} else {
			var obj = new Object();
			obj.state = 'Failed'
				obj.error = 'Error with DB'
		}
	});

}

/*
 * Récupère les balise disponible
 * Donne le nom de la station, son id et son état (active)
 * Tous dans un tableau d'objet JSON
 */
exports.aboutStation = function (req, res) {
	var station_id = req.query.id
		connection.query('SELECT * from station WHERE id=\"' + station_id + '\" LIMIT 1', function (err, rows, fields) {

			if (!err) {
				var result = rows[0];
				if (result == undefined) {
					var obj = new Object();
					obj.state = 'Failed'
						obj.error = 'No matching Id'
						res.end(JSON.stringify(obj));
				} else {

					getLastLocation(station_id, function (latlong) {
						if (latlong.state.toString() === "Success") {
							var lat = latlong.lat
								var long = latlong.long
						} else {
							var lat = ''
								var long = ''
						}

						var obj = new Object();
						obj.state = 'Success'
							obj.error = ''
							obj.battery_percent = '---%' //rows[i].name.toString();
							obj.id = result.id.toString();
						obj.name = result.name.toString();
						obj.active = result.active.toString();
						obj.brand = result.brand.toString();
						obj.model = result.model.toString();
						obj.location = result.location.toString();
						obj.installDate = result.installation_date.toString();
						obj.description = result.description.toString();
						obj.longitude = lat
							obj.latitude = long
							res.end(JSON.stringify(obj));

					});

				}
			} else {
				var obj = new Object();
				obj.state = 'Failed'
					obj.error = 'Error with DB'
					res.end(JSON.stringify(obj));
			}
		});
}

/*
 * Récupère les alertes d'un utilisateur
 * Donne le nombre d,alertes non consultées
 * requiert l'id de l'utilisateur
 */
exports.nbAlerts = function (req, res) {
	var user_id = req.query.id;
		connection.query(
			'SELECT COUNT(alerte.id_alerte) ' +
			'FROM alerte INNER JOIN parametre_alerte ON alerte.id_parametre_alerte = parametre_alerte.id_parametre_alerte ' +
			'WHERE parametre_alerte.id_utilisateur = ' + user_id + ' AND alerte.active = true', function (err, rows, fields) {
			if (!err) {
				var result = rows[0];
				if (result == undefined) {
					var obj = new Object();
					obj.state = 'Failed';
					obj.error = 'No matching Id';
					res.end(JSON.stringify(obj));
				} else {
					var obj = new Object();
					obj.state = 'Success'
						obj.nb = result["COUNT(alerte.id_alerte)"];
					res.end(JSON.stringify(obj));
				}
			} else {
				var obj = new Object();
				obj.state = 'Failed';
				obj.error = 'Error with DB';
				obj.userId = user_id;
				res.end(JSON.stringify(obj));
			}
		});
}/* * Récupère les détails des alertes d'un utilisateur * requiert l'id de l'utilisateur  * TO DO: implement the reason of the alert; the treshold and the comparator type */exports.getAlerts = function (req, res) {	var user_id = req.query.id;	var unread = "";	if (!req.query.unread) {     unread= "`alerte`.`active` = 1 AND";	}			/*			SELECT			  `alerte`.`id_alerte`,			  `sensor_measure`.`date_time`,			  `country`.`name` AS country_name,			  `city`.`name` AS city_name,			  `station`.`name` AS station_name,			  `sensor_type`.`name` AS sensor_name,			  `sensor_measure`.`measure_value`,			  `measure_type`.`name` AS measure_name,			//  `comparateur`.`nom_comparateur`,			//  `critere`.`seuil`,			  `alerte`.`active`			FROM			  `alerte`			LEFT JOIN			  `sensor_measure` ON `sensor_measure`.`id` = `alerte`.`id_measure`			LEFT JOIN			  `measure_type` ON `measure_type`.`id` = `sensor_measure`.`measure_type`			LEFT JOIN			  `parametre_alerte` ON `parametre_alerte`.`id_parametre_alerte` = `alerte`.`id_parametre_alerte`			LEFT JOIN			  `sensor` ON `sensor`.`id` = `sensor_measure`.`sensor_id`			LEFT JOIN			  `sensor_type` ON `sensor_type`.`id` = `sensor`.`sensor_type_id`			LEFT JOIN			  `sensor_unit` ON `sensor_unit`.`id` = `sensor`.`sensor_unit_id`			LEFT JOIN			  `station` ON `station`.`id` = `sensor_unit`.`station_id`			LEFT JOIN			  `city` ON `city`.`id_city` = `station`.`id_city`			LEFT JOIN			  `country` ON `country`.`id_country` = `city`.`id_country`			//LEFT JOIN			//  `critere` ON `critere`.`id_parametre_alerte` = `parametre_alerte`.`id_parametre_alerte`			//LEFT JOIN			//  `comparateur` ON `comparateur`.`id_comparateur` = `critere`.`id_comparateur`			WHERE			  `alerte`.`active` = 1 AND `parametre_alerte`.`id_utilisateur` = 11			*/		connection.query(			'SELECT `alerte`.`id_alerte`, `sensor_measure`.`date_time`, `country`.`name` AS country_name, `city`.`name` AS city_name, `station`.`name` AS station_name, `sensor_type`.`name` AS sensor_name, `sensor_measure`.`measure_value`, `measure_type`.`name` AS measure_name, `alerte`.`active` FROM `alerte` LEFT JOIN `sensor_measure` ON `sensor_measure`.`id` = `alerte`.`id_measure` LEFT JOIN `measure_type` ON `measure_type`.`id` = `sensor_measure`.`measure_type` LEFT JOIN `parametre_alerte` ON `parametre_alerte`.`id_parametre_alerte` = `alerte`.`id_parametre_alerte` LEFT JOIN `sensor` ON `sensor`.`id` = `sensor_measure`.`sensor_id` LEFT JOIN `sensor_type` ON `sensor_type`.`id` = `sensor`.`sensor_type_id` LEFT JOIN `sensor_unit` ON `sensor_unit`.`id` = `sensor`.`sensor_unit_id` LEFT JOIN `station` ON `station`.`id` = `sensor_unit`.`station_id` LEFT JOIN `city` ON `city`.`id_city` = `station`.`id_city` LEFT JOIN `country` ON `country`.`id_country` = `city`.`id_country` WHERE '+unread+' `parametre_alerte`.`id_utilisateur` ='+user_id, function (err, rows, fields) {			if (!err) {				var result = rows;				if (result == undefined) {					var obj = new Object();					obj.state = 'Failed';					obj.error = 'No matching Id';					res.end(JSON.stringify(obj));				} else {					var obj = new Object();					obj.state = 'Success';						obj.result = rows;					res.end(JSON.stringify(obj));				}			} else {				var obj = new Object();				obj.state = 'Failed';				obj.error = err;//'Error with DB';				obj.userId = user_id;				res.end(JSON.stringify(obj));			}		});}

function getUnitArray() {
	var arr = []
	connection.query('SELECT * from measure_type', function (err, rows, fields) {
		if (!err) {
			if (rows.length > 0) {
				for (var i = rows.length - 1; i >= 0; i--) {
					var obj = Object()
						obj.name = rows[i].name.toString()
						obj.description = rows[i].description.toString()
						arr[rows[i].id.toString()] = obj
				}
				console.log("SQL All Right!")
			} else {
				console.log('Aucune unité disponible')
			}
		} else {
			console.log("Impossible d'acceder a la base de donnée")
		}
	});
	return arr
}

function getLastLocation(id, callback) {
	//avec le stationId on va dans le sensor_unit, on obtient le suXXXX id et apres ca on va dans sensor mesure et on obtient les données
	//pour le sensor unit, on recupère ensuite les données par sensor unit qu'on doit trier par date!!!!!!!!!!!!!!!!!!!! Et oui ca m'arrive
	//d'etre frustré dans du code :)

	//On cherche les stations selon l'ID
	connection.query('SELECT * from station WHERE id=\"' + id + '\" LIMIT 1', function (err, rows, fields) {
		if (!err) {
			//On a découvert une balise a l'aide de l'id de celle-ci Incroyable
			if (rows.length > 0) {
				//On détermine le station_unit
				connection.query('SELECT * from sensor_unit WHERE station_id=\"' + id + '\" LIMIT 1', function (err, rows, fields) {
					if (!err) {
						if (rows.length > 0) {

							var stationUnit = rows[0].id

								connection.query('SELECT * from sensor_measure WHERE sensor_id LIKE \'%' + stationUnit + '%\' ORDER BY date_time DESC', function (err, rows, fields) {
									if (!err) {
										if (rows.length > 0) {
											var long = ''
												var lat = ''
												//On sépare les données selon leur tyoe de données, ca fais moins de texte!!
												for (var i = 0; i <= rows.length; i++) {
													if (rows[i] != undefined) {
														if (rows[i].measure_type.toString() === '16') {
															var long = rows[i].measure_value.toString();
															var lat = rows[i + 1].measure_value.toString();
															break;
														}
													}
												}
												if (long === "" || lat === "") {
													lat = ""
														long = ""
												}

												//Les données totale de la station bien organisé, et que selon les dates voulu


												//Les memes données de la station mais cete fois version final avec les info de celle ci
												var objToSend = new Object();
											objToSend.state = 'Success'
												objToSend.error = ''
												objToSend.lat = lat
												objToSend.long = long
												callback(objToSend)
												return objToSend

										} else {
											var obj = new Object();
											obj.state = 'Failed'
												obj.error = 'Aucne donnée disponible'
												return obj;
										}
									} else {
										var obj = new Object();
										obj.state = 'Failed'
											obj.error = 'Error with DB, cannot perform query'
											return obj;
									}
								});
						} else {
							var obj = new Object();
							obj.state = 'Failed'
								obj.error = 'Aucune station unit pour cette balise'
								return obj;
						}
					} else {
						var obj = new Object();
						obj.state = 'Failed'
							obj.error = 'Error with DB, cannot perform query'
							return obj;
					}
				});
			} else {
				var obj = new Object();
				obj.state = 'Failed'
					obj.error = 'Error with DB, cannot perform query'
					return obj;
			}
		} else {
			var obj = new Object();
			obj.state = 'Failed'
				obj.error = 'Error with DB, cannot perform query'
				return obj;
		}
	});
}

//Retourne les pays avec des stations
exports.getStationCountry = function (req, res) {

	connection.query('SELECT * FROM country', function(err, rows, fields){
		if(!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var country = [];
			var id_country = [];
			
			/*id_country.push("");
			country.push("--Country--");*/
			for (var i = 0; i < rows.length; i++) {
				//console.log(rows[i].name);
				id_country.push(rows[i].id_country);
				country.push(rows[i].name);
			}
			obj.country = country;
			obj.id_country = id_country;
			
			res.end(JSON.stringify(obj));
			
		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
	
}

//Get the list of all parameters
exports.getAlertParameters = function (req, res) {
	connection.query('SELECT * FROM `parametre_alerte`', function (err, rows, fields) {
		if (!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var parameterList = [];

			for (var i = 0; i < rows.length; i++) {
				console.log(rows[i].name);
				parameterList.push(rows[i].name);
			}
			obj.parameterList = parameterList;
			res.end(JSON.stringify(obj));

		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		}

	});
	connection.end();
}

//get the list of criterions of a given parameter
exports.getCriterions = function (req, res) {
	var id = req.query.id;
	connection.query('SELECT * FROM `critere` WHERE id_parametre_alerte = ' + id, function (err, rows, fields) {
		if (!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var criterionList = [];

			for (var i = 0; i < rows.length; i++) {
				console.log(rows[i].name);
				criterionList.push(rows[i].name);
			}
			obj.criterionList = criterionList;
			res.end(JSON.stringify(obj));

		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		}

	});
	connection.end();
}

//Retourne les villes selon le pays
exports.getStationCity = function (req, res) {

	var id_country = req.query.id
	
	connection.query('SELECT * FROM city WHERE id_country=\"'+id_country+'\"', function(err, rows, fields){
		if(!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var city = [];
			var id_city = [];
			
			for (var i = 0; i < rows.length; i++) {
				//console.log(rows[i].name);
				id_city.push(rows[i].id_city);
				city.push(rows[i].name);
			}
			
			obj.id_city = id_city;
			obj.city = city;
			res.end(JSON.stringify(obj));
			
		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
}

//Affiche les stations disponibles sont la ville d'un pays
exports.getStations = function (req, res) {

	var id_city = req.query.id
	
	connection.query('SELECT * FROM station WHERE id_city=\"'+id_city+'\"', function(err, rows, fields){
		if(!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var station = [];
			var id_station = []
			
			for (var i = 0; i < rows.length; i++) {
				//console.log(rows[i].name);
				id_station.push(rows[i].id);
				station.push(rows[i].name);
			}
			
			obj.station = station;
			obj.id_station = id_station;
			res.end(JSON.stringify(obj));
			
		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
}

//Permet d'ajouter une station à l'utilisateur.
exports.followStation = function (req, res) {

	var ua_id = req.query.ua_id
	var station_id = req.query.station_id
	access_level_id = 3;
	
	connection.query('INSERT INTO `ta_ua_station` (`ua_id`, `station_id`, `access_level_id`) VALUES (\"'+ua_id+'\", \"'+station_id+'\", \"'+access_level_id+'\")', function(err, rows, fields){
		if(!err) {
			console.log("Follow station Success");
			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			res.end(JSON.stringify(obj));
				
		} else {
			console.log("Follow station failed");
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
}

//permet de créé une station
exports.createStation = function (req, res) {

	var city_id = req.query.city_id
	var station_id = req.query.station_id
	var name = req.query.name;
	var brand = req.query.brand;
	var model = req.query.model;
	var date = req.query.date;
	var location = req.query.location;
	var station_type_id = req.query.station_type_id;
	var station_group = req.query.station_group
	
	active = 1; //Activer par défaut la station à sa création
	
	connection.query('INSERT INTO `station`(`id`, `name`, `model`, `brand`, `installation_date`, `location`, `description`, `station_group_id`, `active`, `station_type_id`, `id_city`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11])', function(err, rows, fields){
		if(!err) {
			console.log("Create station Success");
			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			res.end(JSON.stringify(obj));
				
		} else {
			console.log("Create station failed");
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
}

//Affiche les types de stations disponibles
exports.getStationType = function (req, res) {
	connection.query('SELECT * FROM `station_type`', function(err, rows, fields){
		if(!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var stationType = [];
			var id_station = []
			
			for (var i = 0; i < rows.length; i++) {
				id_station.push(rows[i].id);
				stationType.push(rows[i].name);
			}
			
			obj.stationType = stationType;
			obj.id_station = id_station;
			res.end(JSON.stringify(obj));
			
		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
}

//Affiche les groupes de stations disponibles
exports.getStationGroup = function (req, res) {
	connection.query('SELECT * FROM `station_group`', function(err, rows, fields){
		if(!err) {

			var obj = new Object();
			obj.state = 'Success';
			obj.error = '';
			var stationGroup = [];
			var id_station = []
			
			for (var i = 0; i < rows.length; i++) {
				id_station.push(rows[i].id);
				stationGroup.push(rows[i].name);
			}
			
			obj.stationGroup = stationGroup;
			obj.id_station = id_station;
			res.end(JSON.stringify(obj));
			
		} else {
			var obj = new Object();
			obj.state = 'Failed';
			obj.error = 'Error with DB';
			res.end(JSON.stringify(obj));
		} 

	});
}


// 1 - Check the last update date
// 2 - For each known parameter
// 3 - For each criterion
// 4 - For each new measure from associated sensor in sensor_measure table
// 5 - For each measure, check if it fits the criterion
// 6 - If so, raise an alert
exports.updateAlertTable = function (req, res) {
	console.log('Alert update process started')
	//1 - Check the last update date
	var lastCheck = '';
	fs.readFile(__dirname + '/lastCheck', 'utf8', function (err, data) {
		if (err) {
			console.log('Update error: can\'t read last check');
		} else {
			lastCheck = data;
			// 2 - For each known parameter
			connection.query('SELECT * FROM `parametre_alerte`', function (err, parameterList, fields) {
				if (!err) {
					for (var p = 0; p < parameterList.length; p++) {
						var currentParameter = parameterList[p];
						// 3 - For each criterion
						var parameter_id = currentParameter.id_parametre_alerte;
						connection.query('SELECT * FROM `critere` WHERE id_parametre_alerte = ' + parameter_id, function (err, criterionList, fields) {
							if (!err) {

								for (var c = 0; c < criterionList.length; c++) {
									// 4 - For each new measure from associated sensor in sensor_measure table
									var currentCriterion = criterionList[c];
									var sensor_id = currentCriterion.id_capteur;

									connection.query('SELECT * FROM `sensor_measure` WHERE sensor_id = \'' + sensor_id + '\' AND date_time > STR_TO_DATE( \'' + lastCheck + '\' , \'%Y-%m-%d %T\')', function (err, measureList, fields) {
										if (!err) {

											for (var m = 0; m < measureList.length; m++) {
												var currentMeasure = measureList[m];
												// 5 - For each measure, check if it fits the criterion

												var flag = false; //For the check
												switch (currentCriterion.id_comparateur) {
												case 0:
													//superior
													flag = currentMeasure.measure_value > currentCriterion.seuil;
													break;
												case 1:
													//superior or equal
													flag = currentMeasure.measure_value >= currentCriterion.seuil;
													break;
												case 2:
													//equal
													flag = currentMeasure.measure_value == currentCriterion.seuil;
													break;
												case 3:
													//inferior or equal
													flag = currentMeasure.measure_value <= currentCriterion.seuil;
													break;
												case 4:
													//inferior
													flag = currentMeasure.measure_value < currentCriterion.seuil;
													break;
												default:
													res.end('Error with comparator: ' + currentCriterion.id_comparateur + ' is not a valid option')
												}

												// 6 - if so, raise an alert
												if (flag) {

													//get current date
													var date = new Date();
													var now = date.toJSON().replace('T', ' ').replace(/\..*/, '');

													//get current number of alerts
													connection.query('SELECT MAX(alerte.id_alerte) FROM alerte', function (err, alertList, fields) {
														if (!err) {
															var result = alertList[0];
															var alertNumber = result["MAX(alerte.id_alerte)"];
															var idMeasure = currentMeasure.id;
															alertNumber = alertNumber + 1;
															//insert the alert
															//var insertQuery = 'INSERT INTO alerte (`id_alerte`,`date_activation`,`id_parametre_alerte`,`active`) VALUES('+ alertNumber +', \'' + now + '\' , '+ currentParameter.id_parametre_alerte+ ', 1)';
															var insertQuery = 'INSERT INTO alerte (`id_measure`, `date_activation` , `id_parametre_alerte`,`active`) VALUES(' + idMeasure + ', \'' + now + '\' , ' + currentParameter.id_parametre_alerte + ', 1)';
															connection.query(insertQuery, function (err, rows, fields) {
																if (!err) {
																	//Insert successful

																} else {
																	console.log('Update error: Error with inserting alert : ' + insertQuery);
																}
															});
															alertNumber = alertNumber + 1;
														}
													});
												}
											}
										} else {
											console.log('Update error: Error with getting measures');
										}
									});
								}
							} else {
								console.log('Update error: Error with getting criterions');
							}
						});
					}

					//After everything is checked, update the registered date
					var date = new Date();
					var now = date.toJSON().replace('T', ' ').replace(/\..*/, '');

					fs.writeFile(__dirname + '/lastCheck', now, function (err) {
						if (err) {
							console.log(err);
						} else {
							console.log('Update successful');
						}
					});
				} else {
					console.log('Update error: Error with getting parameters: ' + err);
				}
			});
		}
	});
	//connection.end();
}
