var connection = require('../DB.js');

//
exports.getRestriction = function (req, res) {

	var username = req.query.username;
	var id = req.query.id;
	var obj = new Array();
	var cout = 0;
	var length = 0;

	//Connection a la base de donnée
	connection.query('SELECT * from ta_restriction_sensor WHERE sensor_id=\"' + id + '\"', function (err, rows, fields) {
		if (!err) {

			if (rows.length > 0) {

				length = rows.length;

				//Do for all of the rescriction for a sensor
				for (var i = 0; i < rows.length; i++) {

					var restriction_id = rows[i].sensor_restriction_id.toString();

					//Do a request for the value for the capteur
					connection.query('SELECT * from restriction WHERE id=\"' + restriction_id + '\" LIMIT 1', function (err2, rows2, fields2) {
						if (!err2) {

							var temp = new Object();
							var value = rows2[0].numeric_value.toString();
							var restriction_type_id = rows2[0].restriction_type_id.toString();

							//Do a request for the type of the restriction
							connection.query('SELECT * from restriction_type WHERE id=\"' + restriction_type_id + '\" LIMIT 1', function (err3, rows3, fields3) {

								if (!err3) {

									if (rows3.length > 0) {

										var name_restriction = rows3[0].name.toString();
										var measure_type = rows3[0].measure_type.toString();
										//Do a request for the type of the mesure
										connection.query('SELECT * from measure_type WHERE id=\"' + measure_type + '\" LIMIT 1', function (err4, rows4, fields4) {

											if (!err4) {
												if (rows4.length > 0) {
													var name_mesure = rows4[0].name.toString();
													var description_mesure = rows4[0].description.toString();

													if (cout === 0) {
														var e = new Object();
														e.state = 'Success'
															e.error = '';
														obj.push(e);
													}

													cout++;

													temp.value = 33;
													temp.restrictionType = name_restriction;
													temp.mesureType = measure_type;
													temp.mesureDescription = description_mesure;
													temp.mesureName = name_mesure;

													obj.push(temp);

													if (cout === length) {

														res.end(JSON.stringify(obj));
													}

												} else {
													res.status(404);
													res.end("No mesure type match");
												}
											} else {
												res.end("No data in database");
												res.status(404);
											}

										});
									} else {
										res.status(404);
										res.end("No data in database");
									}
								} else {
									res.status(404);
									res.end("No data in database");
								}
							});

						} else {
							res.status(404);
							res.end("No data in database");
						}

					});

				}

			} else {
				res.end("No data in database");
				res.status(404);
			}

		} else {
			res.end("Error in database");
			res.status(404);
		}

	});

}

//Formate of call
// id = su009_1
// restrictionId = 2 (max temperature)
// value = 20
// active = 1
// stringValue = Max Temperature
// type = Max
// comment = Allo
// descriptionRestriction = Max Temperature

exports.setLimite = function (req, res) {

	var id = req.query.id;
	var restriction = req.query.restrictionId;
	var value = req.query.value;
	var nouveauSensor = false;
	var stringValue = req.query.stringValue;
	var active = req.query.active;
	var typeOfRestriction = req.query.type;
	var comment = req.query.comment;
	var description = req.query.descriptionRestriction;

	//Shearch for the sensor id
	connection.query('SELECT * from ta_restriction_sensor WHERE sensor_id=\"' + id + '\"', function (err, rows, fields) {
		if (!err) {
			if (rows.length > 0) {
				//res.end("Hello");
				for (var i = 0; i < rows.length; i++) {
					nouveauSensor = (rows[i].sensor_restriction_id.toString() == restriction);
				}
				if (nouveauSensor) {
					connection.query('UPDATE restriction SET string_value=\"' + stringValue + '\", numeric_value=\"' + value + '\", Active=\"' + active + '\" LIMIT 1', function (err2, rows2, fields2) {
						if (!err2) {
							console.log(id);
							connection.query('SELECT * from sensor WHERE id=\"' + id + '\" LIMIT 1', function (err3, rows3, fields3) {
								console.log(err3);
								if (!err3) {
									if (rows3.length > 0) {
										var mesureTypeId = rows3[0].measure_type_id.toString();

										connection.query('SELECT * from restriction_type WHERE measure_type=\"' + mesureTypeId + '\" LIMIT 1', function (err4, rows4, fields4) {

											if (!err4) {
												if (rows4.length > 0) {
													//If the restriction type existe
													connection.query('UPDATE restriction_type SET name=\"' + type + '\" WHERE measure_type=\"' + mesureTypeId + '\" LIMIT 1', function (err5, rows5, fields5) {
														if (!err5) {
															res.end("Limit as been update");
														} else {
															res.end("Error 1");
														}
													});

												} else {
													//Add the mesure limit
													connection.query('INSERT INTO restriction_type (`id`, `name`, `description`, `measure_type`) VALUES (\'NULL\', \'' + typeOfRestriction + '\',NULL, \'' + mesureTypeId + '\')', function (err5, rows5, fields5) {

														if (!err5) {
															res.end("Limit as been update");
														} else {
															res.end("Error 2");
														}

													});
												}
											}

										});

									} else {
										res.status(404);
										res.end("No measure id for the sensor");
									}
								} else {
									res.status(404)
									res.end("Error 3");
								}

							});

						} else {
							res.status(404);
							res.end("Error 4");
						}
					});
				} else {
					newStation(id, value, restriction, stringValue, active, typeOfRestriction, comment, description, res);
				}
			} else {
				newStation(id, value, restriction, stringValue, active, typeOfRestriction, comment, description, res);
			}
		} else {
			res.end("Can not log to database");
		}
	});

}

function newStation(id, value, restriction, stringValue, active, typeOfRestriction, comments, description, res) {
	//Get the measure type
	connection.query('SELECT * from sensor WHERE id=\"' + id + '\" LIMIT 1', function (err4, rows4, fields4) {
		if (!err4) {
			if (rows4.length > 0) {
				var mesureTypeId = rows4[0].measure_type_id.toString();

				//Restrition type id
				connection.query('INSERT INTO restriction_type (`id`,`name`, `description`, `measure_type`) VALUES (\'NULL\', \'' + typeOfRestriction + '\',\'' + description + '\',' + mesureTypeId + ')', function (err5, rows5, fields5) {

					if (!err5) {
						//Restriction
						connection.query('INSERT INTO restriction (`id`,`string_value`, `numeric_value`, `comments`, `restriction_type_id`, `Active`) VALUES (\'NULL\', \'' + stringValue + '\',\'' + value + '\',\'' + comments + '\',\'' + rows5.insertId + '\',\'' + active + '\')', function (err3, rows3, fields3) {

							if (!err3) {
								console.log(rows3.insertId);
								console.log(id);
								connection.query('INSERT INTO ta_restriction_sensor (`sensor_id`,`sensor_restriction_id`) VALUES (\'' + id + '\', \'' + rows3.insertId + '\')', function (err2, rows2, fields2) {

									if (!err2) {
										res.end("Limit as been addded");
										res.status(200);
									} else {
										res.end("Error 6");
										res.status(404);
									}

								});

							} else {
								res.end("Error 7");
								res.status(404);
							}

						});

					} else {
						res.end("Error 8");
						res.status(404);
					}

				});
			} else {
				res.end("Error 9");
				res.status(404);
			}
		} else {
			res.end("Error 10");
			res.status(404);
		}

	});
}

// Create a new alert parameter
// 'user' is the id of the user
// 'name' is the name of the new parameter
// the parameter is activated
// the id of the parameter is automagically incremented
exports.newAlertParameter = function (req, res) {
	var name = req.body.name;
	var user = req.body.user;
	var id = 0;

	//Test connection
	connection.query('SELECT MAX(id_parametre_alerte) AS \'max\' FROM parametre_alerte', function (err, rows, fields) {
		if (!err) {
			//convert the result to an usable format
			id = JSON.stringify(rows[0]);
			id = JSON.parse(id);
			id.max = id.max + 1;
			//now id.max is the new id

			//this query inserts the new parameter into the database
			connection.query('INSERT INTO parametre_alerte (`id_parametre_alerte`,`nom_parametre_alerte`,`actif`,`id_utilisateur`) VALUES (' + id.max + ',\'' + name + '\', true, ' + user + ');', function (err2, rows2, fields2) {
				if (!err2) {
					//no problem
					res.end("Success");
				} else {
					//the insert failed
					res.end("Can not update database");
				}
			});
		} else {
			//the select failed
			res.end("Can not log to database");
		}
	});
}

// alter a parameter
// id
// user
// name

exports.alterAlertParameter = function (req, res) {

	var id = req.body.id;
	var name = req.body.name;
	var user = req.body.user;

	//Connection test
	connection.query('SELECT * FROM parametre_alerte WHERE parametre_alerte.id_parametre_alerte = \'' + id + '\' AND actif = 1', function (err, rows, fields) {
		if (!err) {
			//alter parameter with all relevant parameters

			if (req.body.name)
				connection.query('UPDATE parametre_alerte SET nom_parametre_alerte = \'' + name + '\' WHERE id_parametre_alerte = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.user)
				connection.query('UPDATE parametre_alerte SET id_utilisateur = ' + user + ' WHERE id_parametre_alerte = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});

		} else {
			//the select failed
			res.end("Can not find criterion");
		}
	});
}


//// Delete an alert, for real// id: of the alertexports.deleteAlert = function (req, res) {	var id = req.query.id;	//Connection test	connection.query('SELECT * FROM `alerte` WHERE id_alerte = ' + id , function (err, rows, fields) {		if (!err) {			//it's delete time			connection.query('DELETE FROM `alerte` WHERE id_alerte = ' + id , function (err2, rows2, fields2) {				if (!err2) {					//success					res.end("success");				} else {					//the delete failed					res.end("Can not delete alert");				}			});		} else {			//the select failed			res.end("Can not find parameter");		}	});}//// "Delete" a parameter (make it inactive)// idexports.deleteAlertParameter = function (req, res) {	var id = req.body.id;	//Connection test	connection.query('SELECT * FROM parametre WHERE parametre.id_parametre_alerte = \'' + id + '\' AND active = 1', function (err, rows, fields) {		if (!err) {			//alter station with all relevant parameters			connection.query('UPDATE parametre SET active = 0 WHERE id_parametre_alerte = \'' + id + '\';', function (err2, rows2, fields2) {				if (!err2) {					//success					res.end("success");				} else {					//the alter failed					res.end("Can not delete parameter");				}			});		} else {			//the select failed			res.end("Can not find parameter");		}	});}
// Create a new criterion
// id
// start_date
// end_date
// treshold
// start_period
// end_period
// comparator 0: > 1: >= 2: = 3: <= 4: <
// alert_parameter
// sensor

exports.newCriterion = function (req, res) {
	var id = 0;
	var start_date = JSON.stringify(req.body.start_date);
	var end_date = JSON.stringify(req.body.end_date);
	var treshold = req.body.treshold;
	var start_period = JSON.stringify(req.body.start_period);
	var end_period = JSON.stringify(req.body.end_period);
	var comparator = req.body.comparator;
	var alert_parameter = req.body.alert_parameter;
	var sensor = req.body.sensor;

	//Connection test
	connection.query('SELECT MAX(id_critere) AS \'max\' FROM critere', function (err, rows, fields) {
		if (!err) {
			//convert the result to an usable format
			id = JSON.stringify(rows[0]);
			id = JSON.parse(id);
			id.max = id.max + 1;
			//now id.max is the new id

			//this query inserts the new criterion into the database
			var query_insert = 'INSERT INTO critere (`id_critere`,`seuil`,`id_comparateur`,`id_parametre_alerte`,`id_capteur`) VALUES (' + id.max + ',' + treshold + ',' + comparator + ',' + alert_parameter + ',\'' + sensor + '\');';
			connection.query(query_insert, function (err2, rows2, fields2) {
				if (!err2) {
					//the criterion has been created successfully.

					//adding the dates, if necessary

					if (true)
						connection.query('UPDATE critere SET date_debut =' + start_date + ' WHERE id_critere = ' + id.max + ';', function (err3, rows2, fields2) {
							if (!err3)
								res.end("why1");
							else
								res.end("Can not add start date")
						});

					if (end_date)
						connection.query('UPDATE critere SET date_fin =' + end_date + ' WHERE id_critere = ' + id.max + ';', function (err3, rows2, fields2) {
							if (!err3)
								res.end("why2");
							else
								res.end("Can not add end date")
						});

					if (start_period)
						connection.query('UPDATE critere SET periode_debut =' + start_period + ' WHERE id_critere = ' + id.max + ';', function (err3, rows2, fields2) {
							if (!err3)
								res.end("why3");
							else
								res.end("Can not add start period")
						});

					if (end_period)
						connection.query('UPDATE critere SET periode_fin =' + end_period + ' WHERE id_critere = ' + id.max + ';', function (err3, rows2, fields2) {
							if (!err3)
								res.end("why4");
							else
								res.end("Can not add end period")
						});

					res.end("Success");
				} else {
					//the insert failed
					res.end("Can not create criterion: " + query_insert);
				}
			});
		} else {
			//the select failed
			res.end("Can not log to database");
		}
	});
}

// alter a criterion
// id
// start_date
// end_date
// treshold
// start_period
// end_period
// comparator 0: > 1: >= 2: = 3: <= 4: <
// alert_parameter
// sensor

exports.alterCriterion = function (req, res) {
	var id = req.body.id;
	var start_date = JSON.stringify(req.body.start_date);
	var end_date = JSON.stringify(req.body.end_date);
	var treshold = req.body.treshold;
	var start_period = JSON.stringify(req.body.start_period);
	var end_period = JSON.stringify(req.body.end_period);
	var comparator = req.body.comparator;
	var alert_parameter = req.body.alert_parameter;
	var sensor = req.body.sensor;

	//Connection test
	connection.query('SELECT * FROM critere WHERE critere.id_critere = \'' + id + '\' AND actif = 1', function (err, rows, fields) {
		if (!err) {
			//alter criterion with all relevant parameters

			if (req.body.start_date)
				connection.query('UPDATE critere SET date_debut = \'' + start_date + '\' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.end_date)
				connection.query('UPDATE critere SET date_fin = \'' + end_date + '\' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.treshold)
				connection.query('UPDATE critere SET seuil = ' + treshold + ' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.start_period)
				connection.query('UPDATE critere SET periode_debut = \'' + start_period + '\' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.end_period)
				connection.query('UPDATE critere SET periode_fin = \'' + end_period + '\' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.comparator)
				connection.query('UPDATE critere SET id_comparateur = ' + comparator + ' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.alert_parameter)
				connection.query('UPDATE critere SET id_parametre_alerte = ' + alert_parameter + ' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});
			if (req.body.sensor)
				connection.query('UPDATE critere SET id_capteur = \'' + sensor + '\' WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("I can not let you do that, dave");
					}
				});

		} else {
			//the select failed
			res.end("Can not find criterion");
		}
	});
}

//// "Delete" a criterion (make it inactive)
// id

exports.deleteCriterion = function (req, res) {
	var id = req.body.id;

	//Connection test
	connection.query('SELECT * FROM critere WHERE critere.id_critere = \'' + id + '\' AND active = 1', function (err, rows, fields) {
		if (!err) {
			//alter station with all relevant parameters
			connection.query('UPDATE critere SET active = 0 WHERE id_critere = \'' + id + '\';', function (err2, rows2, fields2) {
				if (!err2) {
					//success
					res.end("success");
				} else {
					//the alter failed
					res.end("Can not delete criterion");
				}
			});

		} else {
			//the select failed
			res.end("Can not find criterion");
		}
	});
}

// Alter a station
// id
// name
// model
// brand
// installation_date
// loc - location
// description

exports.alterStation = function (req, res) {
	var id = req.body.id;
	var name = req.body.name;
	var model = req.body.model;
	var brand = req.body.brand;
	var date = JSON.stringify(req.body.installation_date);
	var loc = req.body.loc;
	var description = req.body.description;

	//Connection test
	connection.query('SELECT * FROM station WHERE station.id = \'' + id + '\'', function (err, rows, fields) {
		if (!err) {
			//alter station with all relevant parameters
			if (req.body.name)
				connection.query('UPDATE station SET name =\'' + name + '\' WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("Can not change name");
					}

				});
			if (req.body.model)
				connection.query('UPDATE station SET model =\'' + model + '\' WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("Can not change model");
					}

				});
			if (req.body.brand)
				connection.query('UPDATE station SET brand =\'' + brand + '\' WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("Can not change brand");
					}

				});
			if (req.body.installation_date)
				connection.query('UPDATE station SET installation_date =\'' + date + '\' WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("Can not change installation date");
					}

				});
			if (req.body.loc)
				connection.query('UPDATE station SET location =\'' + loc + '\' WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("Can not change location");
					}

				});
			if (req.body.description)
				connection.query('UPDATE station SET description =\'' + description + '\' WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
					if (!err2) {
						//success
					} else {
						//the alter failed
						res.end("Can not change description");
					}
				});

			res.end("Success");
		} else {
			//the select failed
			res.end("Can not find station");
		}
	});
}

// "Delete" a station (make it inactive)
// id

exports.deleteStation = function (req, res) {
	var id = req.body.id;

	//Connection test
	connection.query('SELECT * FROM station WHERE station.id = \'' + id + '\' AND active = 1', function (err, rows, fields) {
		if (!err) {
			//alter station with all relevant parameters
			connection.query('UPDATE station SET active = 0 WHERE id = \'' + id + '\';', function (err2, rows2, fields2) {
				if (!err2) {
					//success
					res.end("success");
				} else {
					//the alter failed
					res.end("Can not delete station");
				}
			});

		} else {
			//the select failed
			res.end("Can not find station");
		}
	});
}//// Set an alert as read// idexports.readAlert = function (req, res) {	var id = req.query.id;	//Connection test	connection.query('SELECT * FROM alerte WHERE alerte.id_alerte = \'' + id + '\' AND active = 1', function (err, rows, fields) {		if (!err) {			//alter station with all relevant parameters			connection.query('UPDATE alerte SET active = 0 WHERE id_alerte = \'' + id + '\';', function (err2, rows2, fields2) {				if (!err2) {					//success					res.end("success");				} else {					//the update failed					res.end("Can not read alert");				}			});					} else {			//the select failed			res.end("Can not find alerte");		}	});}
