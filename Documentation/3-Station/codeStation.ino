//Alex Morin 2016

//Cloudia project account
//username: loulou784
//Password: Akin0720


//The required library
#include <SPI.h>
#include <SD.h>
#include <Ethernet.h>
#include <SoftwareSerial.h>
#include <Wire.h>

//Library that needs to be downloaded
#include "RTClib.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#include <dht.h>

//The variables
dht DHT;
RTC_DS1307 rtc;

//The mac address of the ethernet shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
EthernetClient client;

//The data that is gooing to be taken by the Arduino on the SD Card
String SSID = "";			//The WIFI network SSID (unused)
String PASS = "";			//The WIFI network password (unused)
String stationName = "";	//The name of the station here: BlackFish
String server = "";			//The server address http://cloudiaproject.org
String host = "";			// /API/receiveData		really importatn that it is written like this...
int PORT = 80;				//The server port address 80 default for website
String time1 = "";			//The first time at which we want to take a reading midnight being 00:00
String time2 = "";			//The second time at which we want to take a reading midnight being 00:00
String time3 = "";			//The third time at which we want to take a reading midnight being 00:00
String stationID = "";		//The name of the station here: bra004
String su = "";				//sensorUnit of format XXXX which are all number

//Number of time we want to take blank reading
short warmup = 20;			//This clears the sensor to get better value

//If we can start the station, the config file is good
bool startStation = false;		//We don't start the station unless all the data on the SD card exist

//If the time is met this is the code to read all the sensor
bool startRoutine = false;		//This get's to true when it's time to take reading

//The time between each reading (60 seconds) AKA a minute
int dataDelay(60000);

//If we want to see the output of the sensor in the serial console
bool DEBUG = true;

//Either we send each reading or send them all at once from the sd card if true: send directly else, log to SD, after 20 reading send all.
bool SEND = true;

//We take reading continuously, ignoring the preset time
bool IGNORETIME = true;

void setup() { 
  // put your setup code here, to run once:
  // Open serial communications and wait for port to open:
  Serial.begin(9600);		//Serial port a 9600 BAUD
  
  pinMode(4,OUTPUT);
  pinMode(10,OUTPUT);

  digitalWrite(10, HIGH);
  
  //We start the sd card shield
  Serial.println("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }

  //If the sd card is present and working we read the config file
  readConfigSD();

  //If the config file is good
  if(startStation == true) {
    digitalWrite(4, HIGH);
    //We start the ethernet shield
    Serial.println("Server:" + server  + host);
    Serial.print(F("Starting ethernet..."));
    if(!Ethernet.begin(mac)) Serial.println(F("failed"));
    else Serial.println(Ethernet.localIP());

    //We startt the real time clock
    if (!rtc.begin()) {
      while (1);
    }
    Serial.println(F("warmup..."));
  } else {
    Serial.println(F("Error with config"));
  }
}

void loop() {
  Ethernet.maintain();
  //If the station can be enabled
  if (startStation) {
    // put your main code here, to run repeatedly:
    short routineCount = 0;
    while(warmup != 0) {
      //We take blank reading every 1 sec for warmup number of start
      Ethernet.maintain();
      getData(12,6,"R", 650);
      getData(11,3,"R", 650);
      getData(13,8,"R", 378);
      warmup--;
      delay(1000);
      Serial.print('.');
      if(warmup == 0) {
        Serial.println(F("done..."));
      }
    }
    //We get the time
    DateTime now = getDateAndTimeRTC();
    String time = "";
    if(now.hour() < 10) {
      time += '0';
      time += now.hour();
    } else {
      time += now.hour();
    }
    time += ':';
    if(now.minute() < 10) {
      time += '0';
      time += now.minute();
    } else {
      time += now.minute();
    }
    //we verifiy if the time is a reading time... if it is we start the data takin code
    if (time == time1 || time == time2 || time == time3 || IGNORETIME == true) {
      startRoutine = true;
    }
    String generatedJSON = "";
    String date = "";

    //If the time is good we take the reading...
    if (startRoutine == true) {
      if(DEBUG) {
        Serial.println(F("EC"));
      }
      //We read each sensor 10 times and create an post request string
      while (routineCount != 10) {
        //Chose a envoyer...
        Ethernet.maintain();
        getCaseTmpAndHum();
        String conductivity = getData(12,6,"R", 650);
        int commaIndex = conductivity.indexOf(',');
        int secondCommaIndex = conductivity.indexOf(',', commaIndex+1);
        generatedJSON = "id="+stationID+"&";
        generatedJSON = "su="+su+"&";
        generatedJSON += "date="+String(getDateAndTimeRTC().year())+"/" + getDateAndTimeRTC().month()+"/" + getDateAndTimeRTC().day() +"&";
        generatedJSON += "time="+String(getDateAndTimeRTC().hour()) + ':' + getDateAndTimeRTC().minute() + ':' + getDateAndTimeRTC().second()+"&";
        generatedJSON += "2="+conductivity.substring(0, commaIndex)+"&";
        generatedJSON += "6="+conductivity.substring(commaIndex+1, secondCommaIndex)+"&";
        generatedJSON += "5="+conductivity.substring(secondCommaIndex+1)+"&";
        generatedJSON += "1="+String(getWaterTemp())+"&";
        generatedJSON += "4="+String(DHT.temperature)+"&";
        generatedJSON += "8="+String(DHT.humidity);
        //We add the string to the SD card
        if(DEBUG) {
        	Serial.println(generatedJSON);
        }
        if(SEND) {
			sendPostETH(server, PORT, host, generatedJSON);
        } else {
        	logToSD(generatedJSON);
        }
        routineCount++;
        delay(dataDelay);
      }
      routineCount = 0;
      //We do the same for the PH and DO
      if(DEBUG) {
      	Serial.println(F("PH-DO"));
  	  }
      while (routineCount != 10) {
        //Chose a envoyer...
        Ethernet.maintain();
        getCaseTmpAndHum();
        generatedJSON = "id="+stationID+"&";
        generatedJSON = "su="+su+"&";
        generatedJSON += "date="+String(getDateAndTimeRTC().year())+"/" + getDateAndTimeRTC().month()+"/" + getDateAndTimeRTC().day() +"&";
        generatedJSON += "time="+String(getDateAndTimeRTC().hour()) + ':' + getDateAndTimeRTC().minute() + ':' + getDateAndTimeRTC().second()+"&";
        generatedJSON += "3="+getData(13,8,"R", 378)+"&";
        generatedJSON += "7="+getData(11,3,"R", 650)+"&";
        generatedJSON += "1="+String(getWaterTemp())+"&";
        generatedJSON += "4="+String(DHT.temperature)+"&";
        generatedJSON += "8="+String(DHT.humidity);
        if(DEBUG) {
        	Serial.println(generatedJSON);
        }
        if(SEND) {
			sendPostETH(server, PORT, host, generatedJSON);
        } else {
        	logToSD(generatedJSON);
        }
        routineCount++;
        delay(dataDelay);
      }
      //We reset the variables so the are back to default
      startRoutine = false;
      routineCount = 0;
      //We try to send the data of the sd card
      if(!SEND) {
      	sendSDContent();
      }
    }
    delay(600);
  }
}

//Reads the config files to get the default 
void readConfigSD() {
  digitalWrite(4,LOW);
  digitalWrite(10,HIGH);
  File myFile;
  Serial.println("Reading config file...");
  // re-open the file for reading:
  myFile = SD.open("config.txt");
  if (myFile) {
    int count = 0;
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      //we separate the key from the value
      String key = myFile.readStringUntil('=');
      String value = myFile.readStringUntil('\n');
      //And for each key, we assign a value
      if(key == F("NAME")) {
        stationName = value;
        count++;
      } else if(key == F("SERVER")) {
        server = value;
        count++;
      } else if(key == F("ID")) {
        stationID = value;
        count++;
      } else if(key == F("HOST")) {
        host = value;
        count++;
      } else if(key == F("TIME1")) {
        time1 = value;
        count++;
      } else if(key == F("TIME2")) {
        time2 = value;
        count++;
      } else if(key == F("TIME3")) {
        time3 = value;
        count++;
      } else if(key == F("SSID")) {
        SSID = value;
        count++;
      } else if(key == F("PASS")) {
        PASS = value;
        count++;
      } else if(key == F("PORT")) {
        PORT = value.toInt();
        count++;
      } else if(key == F("SU")) {
        su = value;
        count++;
      }
     }
    //if we have all the key...
    if (count == 11) {
      //We start the station
      startStation = true;
    }
    myFile.close(); 
  } else {
    Serial.println("Error SD");
  }
}

//get the current time
DateTime getDateAndTimeRTC() {
  return rtc.now();
}

//Get the case temperature and humidity
void getCaseTmpAndHum() {
    DHT.read22(A1);
}

//Get the water temperature
float getWaterTemp() {
  OneWire oneWire(A0);
  DallasTemperature sensors(&oneWire);
  sensors.begin();
  sensors.requestTemperatures(); // Send the command to get temperatures
  return sensors.getTempCByIndex(0);
}

//Writes a string to an sd file
void logToSD(String toLog) {
  digitalWrite(4,LOW);
  digitalWrite(10,HIGH);
  File myFile;
  //Serial.println("Logging -- " + toLog);
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("data.txt", FILE_WRITE);
  // if the file opened okay, write to it:
  if (myFile) {
    myFile.println(toLog);
    // close the file:
    Serial.println("Log successfull");
    myFile.close();
  } else {
    Serial.println("Error SD");
  }
}


//Send the string from the sd card to the server
void sendSDContent() {
  digitalWrite(4,LOW);
  digitalWrite(10,HIGH);
    // re-open the file for reading:
  File myFile = SD.open("data.txt");
  if (myFile) {

    //We read the file entirely
    String fileContent = "";
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      fileContent += (char)myFile.read(); 
    }
    // close the file:

    myFile.close();
    int t = 0;
    int commaIndex = 0;
    String sub = "";
    bool failed = false;
    //We then delete the file
    SD.remove("data.txt");
    //While the content of the string is not empty
    while (fileContent.length() > 0) {
      //we read each line one by one
      commaIndex = fileContent.indexOf('\n');
      sub = fileContent.substring(0, commaIndex);
      fileContent.remove(0, commaIndex + 1);
      //Sending function
      //If the send is successfull, we keep on sending
      if(sendPostETH(server, PORT, host, sub)) {
        //on passe a l'autre
        Serial.println("Sent ->" + sub);
      } else {
        //otherwise, we recreate data.txt, and we put everything back to it
        Serial.println("Writing back to file");
        failed = true;
        break;
      }
 
    }
    if (failed == true) {
    //We put everything back to the SD card
    myFile = SD.open("data.txt", FILE_WRITE);
      if (myFile) {
        myFile.println(sub);
        myFile.println(fileContent);
      } else {
        Serial.println("SD ERROR");
      }
      myFile.close();
    }
  } else {
    // if the file didn't open, print an error:
    Serial.println("SD ERROR");
  }
}


//Read the scientific atlas sensor, using SoftwareSerial using the specified pins. action and delay are from the se
String getData(int RX, int TX, String action, short del){
   SoftwareSerial mySensor(RX, TX);
   mySensor.begin(38400);
   boolean sensor_string_complete = false;
   String sensorstring = "";
   mySensor.print(action);
   mySensor.print('\r');
   delay(del);
   while (mySensor.available() > 0) {
     char inchar = (char)mySensor.read();
     sensorstring += inchar;
     if (inchar == '\r') {
      sensor_string_complete = true;
     }
   }
  if (sensorstring == "check probe\r") {
    return "---";
  }
   return sensorstring;
}

//Function to send data to the server...
//Server -> cloudiaproject.org
//thisPort -> 80
//host -> /API/receiveData
bool sendPostETH(String server,int thisPort,String host,String toSend)
{
  //Use to select between SD card or Ethernet on SPI
  digitalWrite(10,LOW);
  digitalWrite(4,HIGH);
  
  int inChar;
  char outBuf[64];

  char domainBuffer[40];
  server.toCharArray(domainBuffer, 40); 

  char page[40];
  host.toCharArray(page, 40);

  char thisData[300];
  toSend.toCharArray(thisData, 300);

  Serial.print(F("connecting..."));

  if(client.connect(domainBuffer,thisPort) == 1)
  {
    Serial.println(F("connected"));

    // send the header
    sprintf(outBuf,"POST %s HTTP/1.1",page);
    client.println(outBuf);
    sprintf(outBuf,"Host: %s",domainBuffer);
    client.println(outBuf);
    client.println(F("Connection: close\r\nContent-Type: application/x-www-form-urlencoded"));
    //thisData must be urlEncoded
    sprintf(outBuf,"Content-Length: %u\r\n",strlen(thisData));
    client.println(outBuf);

    // send the body (variables)
    client.print(thisData);
  } 
  else
  {
    Serial.println(F("failed"));
    return 0;
  }
  Serial.println();
  Serial.println(F("disconnecting."));
  client.stop();
  return 1;
}


